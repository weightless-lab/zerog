//! A platform agnostic driver to interface with AX5043
//!
//! This driver was built using [`embedded-hal`] traits.
//!
//! [`embedded-hal`]: https://docs.rs/embedded-hal/0.2
//!

#![deny(warnings)]
#![no_std]

use embedded_hal::blocking::delay::DelayMs;
use embedded_hal::blocking::spi::{Transfer, Write};
use embedded_hal::digital::v2::OutputPin;

use heapless::Vec;
use rtt_target::rprintln;

/// AX5x43 driver
pub struct AX5x43<SPI, CS, DELAY> {
    spi: SPI,
    cs: CS,
    delay: DELAY,
    freq: Option<f32>,
    xo_freq: f32,
}

impl<SPI, CS, DELAY, E> AX5x43<SPI, CS, DELAY>
where
    SPI: Transfer<u8, Error = E> + Write<u8, Error = E>,
    CS: OutputPin,
    DELAY: DelayMs<u8>,
{
    /// Creates a new driver from a SPI peripheral and a NCS pin
    pub fn new(spi: SPI, cs: CS, delay: DELAY) -> Self {
        let ax5x43 = AX5x43 {
            spi,
            cs,
            delay,
            freq: None,
            xo_freq: 48.0,
        };
        ax5x43
    }

    /// Reads the AX_REG_SILICONREVISION register; should return `0xC5`
    pub fn who_am_i(&mut self) -> Result<u8, E> {
        self.read_register(Register::AX_REG_SILICONREVISION)
    }

    /// Reads the fast timer registers; (fXTAL / 16) Counter
    pub fn get_timer(&mut self) -> Result<u32, E> {
        let tim0 = self.read_register(Register::AX_REG_TIMER0)?;
        let tim1 = self.read_register(Register::AX_REG_TIMER1)?;
        let tim2 = self.read_register(Register::AX_REG_TIMER2)?;

        Ok(((tim2 as u32) << 16) + ((tim1 as u32) << 8) + tim0 as u32)
    }

    pub fn reset(&mut self) -> Result<(), E> {
        self.cs.set_high().ok();
        self.delay.delay_ms(100);
        self.write_register(Register::AX_REG_PWRMODE, 0xe7)?;
        self.delay.delay_ms(100);
        self.write_register(Register::AX_REG_PWRMODE, 0x67)?;

        Ok(())
    }

    pub fn enable_rx(&mut self) -> Result<(), E> {
        self.write_register(Register::AX_REG_FIFOSTAT, 0x03)?;

        self.write_register(Register::AX_REG_PLLVCOI, 0x99)?;
        self.write_register(Register::AX_REG_PLLRNGCLK, 0x05)?;

        self.write_register(Register::AX_REG_BBOFFSCAP, 0x77)?;
        self.write_register(Register::AX_REG_BBTUNE, 0x03)?;

        self.write_register(Register::AX_REG_TMGRXAGC, 0x00)?;
        self.write_register(Register::AX_REG_TMGRXPREAMBLE1, 0x00)?;
        self.write_register(Register::AX_REG_PKTMISCFLAGS, 0x00)?;
        self.write_register(Register::AX_REG_PWRMODE, 0x69)?;

        self.write_register(Register::AX_REG_IRQMASK0, 0x01)?;
        self.write_register(Register::AX_REG_RADIOEVENTMASK0, 0x00)?;
        Ok(())
    }

    pub fn enable_tx(&mut self) -> Result<(), E> {
        self.write_register(Register::AX_REG_FIFOSTAT, 0x03)?;
        self.write_register(Register::AX_REG_PWRMODE, 0x6d)?;
        Ok(())
    }

    fn set_frequency(&mut self, freq: f32) -> Result<(), E> {
        self.freq = Some(freq);

        let freqa = ((freq / self.xo_freq) * (2_i32.pow(24) as f32) + 0.5) as u32;

        self.write_register(Register::AX_REG_FREQA3, (freqa >> 24) as u8)?;
        self.write_register(Register::AX_REG_FREQA2, (freqa >> 16) as u8)?;
        self.write_register(Register::AX_REG_FREQA1, (freqa >> 8) as u8)?;
        self.write_register(Register::AX_REG_FREQA0, (freqa & 0xff) as u8)?;

        self.write_register(Register::AX_REG_PLLRANGINGA, 0x18)?;

        loop {
            if (self.read_register(Register::AX_REG_PLLRANGINGA)? & 0xc0) == 0xc0 {
                break;
            }
        }

        Ok(())
    }

    pub fn is_fifo_not_empty(&mut self) -> Result<bool, E> {
        Ok((self.read_register(Register::AX_REG_IRQREQUEST0)? & 0x01) == 0x01)
    }

    pub fn read_fifo(&mut self) -> Result<(Vec<u8, 255>, Option<u32>), E> {
        let mut ts = None;
        let mut data: Vec<u8, 255> = Vec::new();

        let fifo_cmd = self.read_register(Register::AX_REG_FIFODATA)?;
        if fifo_cmd == 0xe1 {
            let len = self.read_register(Register::AX_REG_FIFODATA)?;

            let tag = self.read_register(Register::AX_REG_FIFODATA)?;
            assert!(tag == 3, "unexpected tag value ({})", tag);

            for _ in 1..len {
                data.push(self.read_register(Register::AX_REG_FIFODATA)?)
                    .unwrap();
            }
        } else if fifo_cmd == 0x70 {
            let tim2 = self.read_register(Register::AX_REG_FIFODATA)?;
            let tim1 = self.read_register(Register::AX_REG_FIFODATA)?;
            let tim0 = self.read_register(Register::AX_REG_FIFODATA)?;
            ts = Some(((tim2 as u32) << 16) + ((tim1 as u32) << 8) + tim0 as u32);
        } else {
            //rprintln!("unexpected FIFO CMD {:x}", fifo_cmd);
        }
        Ok((data, ts))
    }

    pub fn enter_deepsleep(&mut self) -> Result<(), E> {
        self.write_register(Register::AX_REG_PINFUNCDCLK, 0x00)?;
        self.write_register(Register::AX_REG_FIFOSTAT, 0x03)?;
        self.write_register(Register::AX_REG_PWRMODE, 0x61)?;

        Ok(())
    }

    pub fn enter_standby(&mut self) -> Result<(), E> {
        self.write_register(Register::AX_REG_PINFUNCDCLK, 0x00)?;
        self.write_register(Register::AX_REG_FIFOSTAT, 0x03)?;
        self.write_register(Register::AX_REG_PWRMODE, 0x65)?;

        Ok(())
    }

    pub fn init(&mut self, warfcn: u16) -> Result<(), E> {

        self.write_register(Register::AX_REG_XTALOSC, 0x0D)?;

        self.write_register(Register::AX_REG_XTALAMPL, 0x07)?;

        self.write_register(Register::AX_REG_PLLVCODIV, 0x20)?;

        /* decoupling capa 184 8 + 0.5*X */
        self.write_register(Register::AX_REG_XTALCAP, 0x01)?;

        /* second step configure PLL and VCO */

        self.write_register(Register::_f34, 0x08)?;

        self.set_frequency(warfcn as f32 / 10.0)?;

        /* set gfsk */
        self.write_register(Register::AX_REG_MODULATION, 0x8)?;

        /* set encoding correspond to NRZ */
        self.write_register(Register::AX_REG_ENCODING, 0x0)?;

        /* set framing to raw and pattern matching */
        self.write_register(Register::AX_REG_FRAMING, 0x6)?;
        Ok(())
    }

    pub fn perf_registers(&mut self) -> Result<(), E> {
        self.write_register(Register::_f00, 0x0f)?;
        self.write_register(Register::AX_REG_POWCTRL, 0x04)?;
        self.write_register(Register::_f18, 0x06)?;
        self.write_register(Register::AX_REG_REF, 0x03)?;

        self.write_register(Register::_f1c, 0x07)?;
        self.write_register(Register::_f21, 0x68)?;
        self.write_register(Register::_f22, 0xff)?;
        self.write_register(Register::_f23, 0x84)?;
        self.write_register(Register::_f26, 0x98)?;
        self.write_register(Register::_f35, 0x11)?;
        self.write_register(Register::_f44, 0x25)?;

        self.write_register(Register::AX_REG_MODCFGP, 0xe7)?;

        Ok(())
    }

    pub fn pattern_matching(&mut self, preamble : &[u8;2], syncword : &[u8;3]) -> Result<(), E> {
        /* sync word pattern */
        self.write_register(Register::AX_REG_MATCH0PAT3, syncword[2])?;
        self.write_register(Register::AX_REG_MATCH0PAT2, syncword[1])?;
        self.write_register(Register::AX_REG_MATCH0PAT1, syncword[0])?;
        self.write_register(Register::AX_REG_MATCH0PAT0, 0x00)?;

        /* sync word length / raw */
        self.write_register(Register::AX_REG_MATCH0LEN, 0x97)?;

        /* sync word detection criteria */
        self.write_register(Register::AX_REG_MATCH0MIN, 0x00)?;
        self.write_register(Register::AX_REG_MATCH0MAX, 0x14)?;

        /* preamble pattern */
        self.write_register(Register::AX_REG_MATCH1PAT1, preamble[1])?;
        self.write_register(Register::AX_REG_MATCH1PAT0, preamble[0])?;

        /* preamble length / raw */
        self.write_register(Register::AX_REG_MATCH1LEN, 0x8f)?;

        /* preamble detection criteria */
        self.write_register(Register::AX_REG_MATCH1MIN, 0x00)?;
        self.write_register(Register::AX_REG_MATCH1MAX, 0x0a)?;

        Ok(())
    }

    pub fn packet_controller(&mut self) -> Result<(), E> {
        self.write_register(Register::AX_REG_PKTADDRCFG, 0x80)?;
        self.write_register(Register::AX_REG_PKTLENOFFSET, 0x3f)?;
        self.write_register(Register::AX_REG_PKTLENCFG, 0x00)?;
        self.write_register(Register::AX_REG_PKTMAXLEN, 0xC8)?;
        self.write_register(Register::AX_REG_PKTCHUNKSIZE, 0x3c)?;
        self.write_register(Register::AX_REG_PKTACCEPTFLAGS, 0x3f)?;
        self.write_register(Register::AX_REG_PKTSTOREFLAGS, 0x01)?;
        self.write_register(Register::AX_REG_PKTMISCFLAGS, 0x04)?;

        self.write_register(Register::AX_REG_TMGTXBOOST, 0x5b)?;
        self.write_register(Register::AX_REG_TMGTXSETTLE, 0x3e)?;

        self.write_register(Register::AX_REG_TMGRXBOOST, 0x5b)?;
        self.write_register(Register::AX_REG_TMGRXSETTLE, 0x3e)?;
        self.write_register(Register::AX_REG_TMGRXCOARSEAGC, 0x9c)?;
        self.write_register(Register::AX_REG_TMGRXOFFSACQ, 0x00)?;
        self.write_register(Register::AX_REG_TMGRXRSSI, 0x03)?;
        self.write_register(Register::AX_REG_TMGRXPREAMBLE2, 0x35)?;

        Ok(())
    }

    pub fn rx_parameters(&mut self) -> Result<(), E> {
        /* frequency shaping bt=0.3 */
        self.write_register(Register::AX_REG_MODCFGF, 0x02)?;

        let datarate = 100_000.0;

        let decimation = 2;
        let xtaldiv = 2;

        self.write_register(Register::AX_REG_IFFREQ1, 0x0B)?;
        self.write_register(Register::AX_REG_IFFREQ0, 0x09)?;
        self.write_register(Register::AX_REG_DECIMATION, 0x02)?;

        /* RX data rate */
        let rxdatarate = (((2_i32.pow(7) as f32) * self.xo_freq * 1_000_000.0
            / (decimation as f32 * xtaldiv as f32 * datarate))
            + 0.5) as u32;

        self.write_register(Register::AX_REG_RXDATARATE2, (rxdatarate >> 16) as u8)?;
        self.write_register(Register::AX_REG_RXDATARATE1, (rxdatarate >> 8) as u8)?;
        self.write_register(Register::AX_REG_RXDATARATE0, (rxdatarate & 0xff) as u8)?;

        /* Max Data rate offset */
        self.write_register(Register::AX_REG_MAXDROFFSET2, 0x00)?;
        self.write_register(Register::AX_REG_MAXDROFFSET1, 0x00)?;
        self.write_register(Register::AX_REG_MAXDROFFSET0, 0x00)?;

        /* assume we have only 10ppm oscillators */
        let max_rf_offset = ((self.freq.unwrap() * 10.0 * 2.0) * 2_i32.pow(24) as f32
            / (self.xo_freq * 1_000_000.0)
            + 0.5) as u32;

        self.write_register(Register::AX_REG_MAXRFOFFSET2, 0x80)?;
        self.write_register(Register::AX_REG_MAXRFOFFSET1, (max_rf_offset >> 8) as u8)?;
        self.write_register(Register::AX_REG_MAXRFOFFSET0, (max_rf_offset & 0xff) as u8)?;

        self.write_register(Register::AX_REG_RXPARAMSETS, 0xf4)?;

        self.write_register(Register::AX_REG_RX_PARAMETER0, 0x83)?;

        self.write_register(Register::AX_121, 0x84)?;
        //self.write_register(Register::AX_122, 0x00)?;
        //self.write_register(Register::AX_123, 0x00)?;

        self.write_register(Register::AX_124, 0xf8)?;
        self.write_register(Register::AX_125, 0xf2)?;
        self.write_register(Register::AX_126, 0xc3)?;

        self.write_register(Register::AX_127, 0x0f)?;
        self.write_register(Register::AX_128, 0x1f)?;
        self.write_register(Register::AX_129, 0x05)?;
        self.write_register(Register::AX_12a, 0x05)?;

        self.write_register(Register::AX_12b, 0x06)?;

        self.write_register(Register::AX_12c, 0x00)?;
        self.write_register(Register::AX_12d, 0x00)?;

        //self.write_register(Register::AX_12e, 0x00);
        self.write_register(Register::AX_12f, 0x00)?;

        self.write_register(Register::AX_REG_RX_PARAMETER1, 0x83)?;

        self.write_register(Register::AX_131, 0x84)?;
        self.write_register(Register::AX_132, 0x00)?;
        self.write_register(Register::AX_133, 0x00)?;

        self.write_register(Register::AX_134, 0xf6)?;
        self.write_register(Register::AX_135, 0xf1)?;
        self.write_register(Register::AX_136, 0xc3)?;

        self.write_register(Register::AX_137, 0x0f)?;
        self.write_register(Register::AX_138, 0x1f)?;
        self.write_register(Register::AX_139, 0x05)?;
        self.write_register(Register::AX_13a, 0x05)?;

        self.write_register(Register::AX_13b, 0x06)?;

        self.write_register(Register::AX_13c, 0x00)?;
        self.write_register(Register::AX_13d, 0x25)?;

        self.write_register(Register::AX_13e, 0x16)?;
        self.write_register(Register::AX_13f, 0x00)?;

        self.write_register(Register::AX_REG_RX_PARAMETER3, 0xff)?;

        self.write_register(Register::AX_151, 0x84)?;
        self.write_register(Register::AX_152, 0x00)?;
        self.write_register(Register::AX_153, 0x00)?;

        self.write_register(Register::AX_154, 0xf5)?;
        self.write_register(Register::AX_155, 0xf0)?;
        self.write_register(Register::AX_156, 0xc3)?;

        self.write_register(Register::AX_157, 0x0f)?;
        self.write_register(Register::AX_158, 0x1f)?;
        self.write_register(Register::AX_159, 0x09)?;
        self.write_register(Register::AX_15a, 0x09)?;

        self.write_register(Register::AX_15b, 0x06)?;

        self.write_register(Register::AX_15c, 0x00)?;
        self.write_register(Register::AX_15d, 0x25)?;

        self.write_register(Register::AX_15e, 0x16)?;
        self.write_register(Register::AX_15f, 0x00)?;

        self.write_register(Register::AX_REG_PLLLOOP, 0x09)?;
        self.write_register(Register::AX_REG_PLLCPI, 0x01)?;
        self.write_register(Register::_f18, 0x06)?;

        Ok(())
    }

    pub fn tx(&mut self, bytes:&[u8]) -> Result<(), E> {
        for byte in bytes {
            self.write_register(Register::AX_REG_FIFODATA, *byte)?;
        }

        self.write_register(Register::AX_REG_FIFOSTAT, 0x04)?;

        Ok(())
    }

    pub fn tx_parameters(&mut self) -> Result<(), E> {
        /* frequency shaping bt=0.3 */
        self.write_register(Register::AX_REG_MODCFGF, 0x02)?;

        /* TX rate */

        let tx_rate: f32 = (100_000 as f32 / 16_000_000 as f32) * (2_i32.pow(24) as f32) + 0.5;

        let reg_val = tx_rate as u32;
        rprintln!("tx_rate {} {:x}", tx_rate, reg_val);

        self.write_register(Register::AX_REG_TXRATE2, (reg_val >> 16) as u8)?;
        self.write_register(Register::AX_REG_TXRATE1, (reg_val >> 8) as u8)?;
        self.write_register(Register::AX_REG_TXRATE0, (reg_val & 0xff) as u8)?;

        /* frequency deviation */

        let reg_val = (tx_rate / 4.0) as u32;
        rprintln!("freq dev {} {:x}", tx_rate, reg_val);

        self.write_register(Register::AX_REG_FSKDEV2, (reg_val >> 16) as u8)?;
        self.write_register(Register::AX_REG_FSKDEV1, (reg_val >> 8) as u8)?;
        self.write_register(Register::AX_REG_FSKDEV0, (reg_val & 0xff) as u8)?;

        self.write_register(Register::AX_REG_MODCFGA, 0x05)?;

        /* power coeff B */
        self.write_register(Register::AX_REG_TXPWRCOEFFB1, 0xf)?;
        self.write_register(Register::AX_REG_TXPWRCOEFFB0, 0xff)?;
        Ok(())
    }

    fn read_register(&mut self, reg: Register) -> Result<u8, E> {
        let addr = reg.addr();

        let _ = self.cs.set_low();
        let val = if addr > 0x70 {
            let mut buffer = [((addr + 0x7000) >> 8) as u8, (addr & 0xff) as u8, 0x00];
            self.spi.transfer(&mut buffer)?;
            buffer[2]
        } else {
            let mut buffer = [addr as u8, 0x00];
            self.spi.transfer(&mut buffer)?;
            buffer[1]
        };
        let _ = self.cs.set_high();

        Ok(val)
    }

    fn write_register(&mut self, reg: Register, val: u8) -> Result<(), E> {
        let addr = reg.addr();

        let _ = self.cs.set_low();
        if addr > 0x70 {
            let buffer = [((addr + 0xf000) >> 8) as u8, (addr & 0xff) as u8, val];
            self.spi.write(&buffer)?;
        } else {
            let buffer = [(addr + 0x80) as u8, val];
            self.spi.write(&buffer)?;
        }
        let _ = self.cs.set_high();

        Ok(())
    }
}

#[allow(dead_code)]
#[allow(non_camel_case_types)]
#[rustfmt::skip]
enum Register {
    AX_REG_SILICONREVISION = 0x000, /* Silicon Revision */
    AX_REG_SCRATCH = 0x001,         /* Scratch */
    AX_REG_PWRMODE = 0x002,         /* Power Mode */

    /**
     * Voltage Regulator
     */
    AX_REG_POWSTAT = 0x003,       /* Power Management Status */
    AX_REG_POWSTICKYSTAT = 0x004, /* Power Management Sticky Status */
    AX_REG_POWIRQMASK = 0x005,    /* Power Management Interrupt Mask */

    /**
     * Interrupt control
     */
    AX_REG_IRQMASK1 = 0x006,        /* IRQ Mask */
    AX_REG_IRQMASK0 = 0x007,        /* IRQ Mask */
    AX_REG_RADIOEVENTMASK1 = 0x008, /* Radio Event Mask */
    AX_REG_RADIOEVENTMASK0 = 0x009, /* Radio Event Mask */
    AX_REG_IRQINVERSION = 0x00A,    /* IRQ Inversion */
    AX_REG_IRQREQUEST1 = 0x00C,     /* IRQ Request */
    AX_REG_IRQREQUEST0 = 0x00D,     /* IRQ Request */
    AX_REG_RADIOEVENTREQ1 = 0x00E,  /* Radio Event Request */
    AX_REG_RADIOEVENTREQ0 = 0x00F,  /* Radio Event Request */

    /**
     * Modulation & Framing
     */
    AX_REG_MODULATION = 0x010, /* Modulation */
    AX_REG_ENCODING = 0x011,   /* Encoding */
    AX_REG_FRAMING = 0x012,    /* Framing Mode */
    AX_REG_CRCINIT = 0x014,    /* CRC Initial Value */

    /**
     * FEC
     */
    AX_REG_FEC = 0x018,       /* Forward Error Correction */
    AX_REG_FECSYNC = 0x019,   /* Forward Error Correction Sync Threshold */
    AX_REG_FECSTATUS = 0x01A, /* Forward Error Correction Status */

    /**
     * Status
     */
    AX_REG_RADIOSTATE = 0x01C, /* Radio Controller State */
    AX_REG_XTALSTATUS = 0x01D, /* Crystal Oscillator Status */

    /**
     * Pin Configuration
     */
    AX_REG_PINSTATE = 0x020,      /* Pin State */
    AX_REG_PINFUNCSYSCLK = 0x021, /* Pin Function SYSCLK */
    AX_REG_PINFUNCDCLK = 0x022,   /* Pin Function DCLK */
    AX_REG_PINFUNCDATA = 0x023,   /* Pin Function DATA */
    AX_REG_PINFUNCIRQ = 0x024,    /* Pin Function IRQ */
    AX_REG_PINFUNCANTSEL = 0x025, /* Pin Function ANTSEL */
    AX_REG_PINFUNCPWRAMP = 0x026, /* Pin Function PWRAMP */
    AX_REG_PWRAMP = 0x027,        /* PWRAMP Control */

    /**
     * FIFO
     */
    AX_REG_FIFOSTAT = 0x028,   /* FIFO Control */
    AX_REG_FIFODATA = 0x029,   /* FIFO Data */
    AX_REG_FIFOCOUNT1 = 0x02A, /* Number of Words currently in FIFO */
    AX_REG_FIFOCOUNT0 = 0x02B, /* Number of Words currently in FIFO */
    AX_REG_FIFOFREE = 0x02C,   /* Number of Words that can be written to FIFO */
    AX_REG_FIFOTHRESH = 0x02E, /* FIFO Threshold */

    /**
     * Synth
     */
    AX_REG_PLLLOOP = 0x030,      /* PLL Loop Filter Settings */
    AX_REG_PLLCPI = 0x031,       /* PL Charge Pump Current */
    AX_REG_PLLVCODIV = 0x032,    /* PLL Divider Settings */
    AX_REG_PLLRANGINGA = 0x033,  /* PLL Autoranging A */
    AX_REG_FREQA3 = 0x034,       /* Frequency A */
    AX_REG_FREQA2 = 0x035,       /* Frequency A */
    AX_REG_FREQA1 = 0x036,       /* Frequency A */
    AX_REG_FREQA0 = 0x037,       /* Frequency A */
    AX_REG_PLLLOOPBOOST = 0x038, /* PLL Loop Filter Settings (Boosted) */
    AX_REG_PLLCPIBOOST = 0x039,  /* PLL Charge Pump Current (Boosted) */
    AX_REG_PLLRANGINGB = 0x03B,  /* PLL Autoranging B */
    AX_REG_FREQB = 0x03C,        /* Frequency B */

    /**
     * Signal strength
     */
    AX_REG_RSSI = 0x040,       /* Received Signal Strength Indicator */
    AX_REG_BGNDRSSI = 0x041,   /* Background RSSI */
    AX_REG_DIVERSITY = 0x042,  /* Antenna Diversity Configuration */
    AX_REG_AGCCOUNTER = 0x043, /* AGC Counter */

    /**
     * Rx tracking
     */
    AX_REG_TRKDATARATE2 = 0x045, /* Datarate Tracking */
    AX_REG_TRKDATARATE1 = 0x046, /* Datarate Tracking */
    AX_REG_TRKDATARATE0 = 0x047, /* Datarate Tracking */

    AX_REG_TRKAMPLITUDE = 0x048, /* Amplitude Tracking */
    AX_REG_TRKPHASE = 0x04A,     /* Phase Tracking */
    AX_REG_TRKRFFREQ2 = 0x04D,   /* RF Frequency Tracking */
    AX_REG_TRKRFFREQ1 = 0x04E,   /* RF Frequency Tracking */
    AX_REG_TRKRFFREQ0 = 0x04F,   /* RF Frequency Tracking */
    AX_REG_TRKFREQ1 = 0x050,     /* Frequency Tracking */
    AX_REG_TRKFREQ0 = 0x051,     /* Frequency Tracking */
    AX_REG_TRKFSKDEMOD = 0x052,  /* FSK Demodulator Tracking */
    AX_REG_TRKAFSKDEMOD = 0x054, /* AFSK Demodulator Tracking */

    /**
     * Timer
     */
    AX_REG_TIMER2 = 0x059, /* 1MHz Timer */
    AX_REG_TIMER1 = 0x05A, /* 1MHz Timer */
    AX_REG_TIMER0 = 0x05B, /* 1MHz Timer */

    AX_REG_WAKEUPTIMER1 = 0x068, /* Wakeup Timer */
    AX_REG_WAKEUPTIMER0 = 0x069, /* Wakeup Timer */

    AX_REG_WAKEUP1 = 0x06A, /* Wakeup Time */
    AX_REG_WAKEUP0 = 0x06B, /* Wakeup Time */

    AX_REG_WAKEUPFREQ1 = 0x06C, /* Wakeup Frequency */
    AX_REG_WAKEUPFREQ0 = 0x06D, /* Wakeup Frequency */

    AX_REG_WAKEUPXOEARLY = 0x06E, /* Wakeup Crystal Oscillator Early */

    /**
     * Receiver parameters
     */
    AX_REG_IFFREQ1 = 0x100,       /* 2nd LO / IF Frequency */
    AX_REG_IFFREQ0 = 0x101,       /* 2nd LO / IF Frequency */
    AX_REG_DECIMATION = 0x102,    /* Decimation Factor */
    AX_REG_RXDATARATE2 = 0x103,   /* Receiver Datarate */
    AX_REG_RXDATARATE1 = 0x104,   /* Receiver Datarate */
    AX_REG_RXDATARATE0 = 0x105,   /* Receiver Datarate */
    AX_REG_MAXDROFFSET2 = 0x106,  /* Maximum Receiver Datarate Offset */
    AX_REG_MAXDROFFSET1 = 0x107,  /* Maximum Receiver Datarate Offset */
    AX_REG_MAXDROFFSET0 = 0x108,  /* Maximum Receiver Datarate Offset */
    AX_REG_MAXRFOFFSET2 = 0x109,  /* Maximum Receiver RF Offset */
    AX_REG_MAXRFOFFSET1 = 0x10a,  /* Maximum Receiver RF Offset */
    AX_REG_MAXRFOFFSET0 = 0x10b,  /* Maximum Receiver RF Offset */
    AX_REG_FSKDMAX1 = 0x10C,      /* Four FSK Rx Maximum Deviation */
    AX_REG_FSKDMAX0 = 0x10d,      /* Four FSK Rx Maximum Deviation */
    AX_REG_FSKDMIN1 = 0x10E,      /* Four FSK Rx Minimum Deviation */
    AX_REG_FSKDMIN0 = 0x10f,      /* Four FSK Rx Minimum Deviation */
    AX_REG_AFSKSPACE = 0x110,     /* AFSK Space (0) Frequency */
    AX_REG_AFSKMARK = 0x112,      /* AFSK Mark (1) Frequency */
    AX_REG_AFSKCTRL = 0x114,      /* AFSK Control */
    AX_REG_AMPLFILTER = 0x115,    /* Amplitude Filter */
    AX_REG_FREQUENCYLEAK = 0x116, /* Baseband Frequency Recovery Loop Leakiness */
    AX_REG_RXPARAMSETS = 0x117,   /* Receiver Parameter Set Indirection */
    AX_REG_RXPARAMCURSET = 0x118, /* Receiver Parameter Current Set */

    /**
     * Receiver Parameter Sets
     */
    AX_REG_RX_PARAMETER0 = 0x120,
    AX_121 = 0x121,
    AX_122 = 0x122,
    AX_123 = 0x123,
    AX_124 = 0x124,
    AX_125 = 0x125,
    AX_126 = 0x126,
    AX_127 = 0x127,
    AX_128 = 0x128,
    AX_129 = 0x129,
    AX_12a = 0x12a,
    AX_12b = 0x12b,

    AX_12c = 0x12c,
    AX_12d = 0x12d,

    AX_12e = 0x12e,
    AX_12f = 0x12f,

    AX_REG_RX_PARAMETER1 = 0x130,
    AX_131 = 0x131,
    AX_132 = 0x132,
    AX_133 = 0x133,
    AX_134 = 0x134,
    AX_135 = 0x135,
    AX_136 = 0x136,
    AX_137 = 0x137,
    AX_138 = 0x138,
    AX_139 = 0x139,
    AX_13a = 0x13a,
    AX_13b = 0x13b,

    AX_13c = 0x13c,
    AX_13d = 0x13d,

    AX_13e = 0x13e,
    AX_13f = 0x13f,

    AX_REG_RX_PARAMETER3 = 0x150,
    AX_151 = 0x151,
    AX_152 = 0x152,
    AX_153 = 0x153,
    AX_154 = 0x154,
    AX_155 = 0x155,
    AX_156 = 0x156,
    AX_157 = 0x157,
    AX_158 = 0x158,
    AX_159 = 0x159,
    AX_15a = 0x15a,
    AX_15b = 0x15b,

    AX_15c = 0x15c,
    AX_15d = 0x15d,

    AX_15e = 0x15e,
    AX_15f = 0x15f,

    /**
     * Transmitter Parameters
     */
    AX_REG_MODCFGF = 0x160,      /* Modulator Configuration F */
    AX_REG_FSKDEV2 = 0x161,      /* FSK Deviation */
    AX_REG_FSKDEV1 = 0x162,      /* FSK Deviation */
    AX_REG_FSKDEV0 = 0x163,      /* FSK Deviation */
    AX_REG_MODCFGA = 0x164,      /* Modulator Configuration A */
    AX_REG_TXRATE2 = 0x165,      /* Transmitter Bitrate */
    AX_REG_TXRATE1 = 0x166,      /* Transmitter Bitrate */
    AX_REG_TXRATE0 = 0x167,      /* Transmitter Bitrate */
    AX_REG_TXPWRCOEFFA = 0x168,  /* Transmitter Predistortion Coefficient A */
    AX_REG_TXPWRCOEFFB1 = 0x16A, /* Transmitter Predistortion Coefficient B */
    AX_REG_TXPWRCOEFFB0 = 0x16B, /* Transmitter Predistortion Coefficient B */
    AX_REG_TXPWRCOEFFC = 0x16C,  /* Transmitter Predistortion Coefficient C */
    AX_REG_TXPWRCOEFFD = 0x16E,  /* Transmitter Predistortion Coefficient D */
    AX_REG_TXPWRCOEFFE = 0x170,  /* Transmitter Predistortion Coefficient E */

    /**
     * PLL Paramters
     */
    AX_REG_PLLVCOI = 0x180,    /* PLL VCO Current */
    AX_REG_PLLVCOIR = 0x181,   /* PLL VCO Current Readback */
    AX_REG_PLLLOCKDET = 0x182, /* PLL Lock Detect Delay */
    AX_REG_PLLRNGCLK = 0x183,  /* PLL Autoranging Clock */

    /**
     * Crystal Oscillator
     */
    AX_REG_XTALCAP = 0x184, /* Crystal Oscillator Load Capacitance */

    /**
     * Baseband
     */
    AX_REG_BBTUNE = 0x188,    /* Baseband Tuning */
    AX_REG_BBOFFSCAP = 0x189, /* Baseband Offset Compensation Capacitors */

    /**
     * Packet format
     */
    AX_REG_PKTADDRCFG = 0x200,   /* Packet Address Config */
    AX_REG_PKTLENCFG = 0x201,    /* Packet Length Configuration */
    AX_REG_PKTLENOFFSET = 0x202, /* Packet Length Offset */
    AX_REG_PKTMAXLEN = 0x203,    /* Packet Maximum Length */
    AX_REG_PKTADDR = 0x204,      /* Packet Address */
    AX_REG_PKTADDRMASK = 0x208,  /* Packet Address Mask */

    /**
     * Pattern match
     */
    AX_REG_MATCH0PAT3 = 0x210, /* Pattern Match Unit 0, Pattern */
    AX_REG_MATCH0PAT2 = 0x211, /* Pattern Match Unit 0, Pattern */
    AX_REG_MATCH0PAT1 = 0x212, /* Pattern Match Unit 0, Pattern */
    AX_REG_MATCH0PAT0 = 0x213, /* Pattern Match Unit 0, Pattern */
    AX_REG_MATCH0LEN = 0x214,  /* Pattern Match Unit 0, Pattern Length */
    AX_REG_MATCH0MIN = 0x215,  /* Pattern Match Unit 0, Minimum Match */
    AX_REG_MATCH0MAX = 0x216,  /* Pattern Match Unit 0, Maximum Match */
    AX_REG_MATCH1PAT1 = 0x218, /* Pattern Match Unit 1, Pattern */
    AX_REG_MATCH1PAT0 = 0x219, /* Pattern Match Unit 1, Pattern */
    AX_REG_MATCH1LEN = 0x21C,  /* Pattern Match Unit 1, Pattern Length */
    AX_REG_MATCH1MIN = 0x21D,  /* Pattern Match Unit 1, Minimum Match */
    AX_REG_MATCH1MAX = 0x21E,  /* Pattern Match Unit 1, Maximum Match */

    /**
     * Packet controller
     */
    AX_REG_TMGTXBOOST = 0x220,     /* Transmit PLL Boost Time */
    AX_REG_TMGTXSETTLE = 0x221,    /* Transmit PLL (post Boost) Settling Time */
    AX_REG_TMGRXBOOST = 0x223,     /* Receive PLL Boost Time */
    AX_REG_TMGRXSETTLE = 0x224,    /* Receive PLL (post Boost) Settling Time */
    AX_REG_TMGRXOFFSACQ = 0x225,   /* Receive Baseband DC Offset Acquisition Time */
    AX_REG_TMGRXCOARSEAGC = 0x226, /* Receive Coarse AGC Time */
    AX_REG_TMGRXAGC = 0x227,       /* Receiver AGC Settling Time */
    AX_REG_TMGRXRSSI = 0x228,      /* Receiver RSSI Settling Time */
    AX_REG_TMGRXPREAMBLE1 = 0x229, /* Receiver Preamble 1 Timeout */
    AX_REG_TMGRXPREAMBLE2 = 0x22A, /* Receiver Preamble 2 Timeout */
    AX_REG_TMGRXPREAMBLE3 = 0x22B, /* Receiver Preamble 3 Timeout */
    AX_REG_RSSIREFERENCE = 0x22C,  /* RSSI Offset */
    AX_REG_RSSIABSTHR = 0x22D,     /* RSSI Absolute Threshold */
    AX_REG_BGNDRSSIGAIN = 0x22E,   /* Background RSSI Averaging Time Constant */
    AX_REG_BGNDRSSITHR = 0x22F,    /* Background RSSI Relative Threshold */
    AX_REG_PKTCHUNKSIZE = 0x230,   /* Packet Chunk Size */
    AX_REG_PKTMISCFLAGS = 0x231,   /* Packet Controller Miscellaneous Flags */
    AX_REG_PKTSTOREFLAGS = 0x232,  /* Packet Controller Store Flags */
    AX_REG_PKTACCEPTFLAGS = 0x233, /* Packet Controller Accept Flags */

    /**
     * GPADC
     */
    AX_REG_GPADCCTRL = 0x300,    /* General Purpose ADC Control */
    AX_REG_GPADCPERIOD = 0x301,  /* GPADC Sampling Period */
    AX_REG_GPADC13VALUE = 0x308, /* GPADC13 Value */

    /**
     * LPOSC Calibration
     */
    AX_REG_LPOSCCONFIG = 0x310, /* LPOSC Calibration Configuration */
    AX_REG_LPOSCSTATUS = 0x311, /* LPOSC Calibration Status */
    AX_REG_LPOSCKFILT = 0x312,  /* LPOSC Calibration Filter Constant */
    AX_REG_LPOSCREF = 0x314,    /* LPOSC Reference Frequency */
    AX_REG_LPOSCFREQ = 0x316,   /* LPOSC Frequency Tuning */
    AX_REG_LPOSCPER = 0x318,    /* LPOSC Period */

    /**
     * DAC
     */
    AX_REG_DACVALUE = 0x330,  /* DAC Value */
    AX_REG_DACCONFIG = 0x332, /* DAC Configuration */

    /**
     * Performance Tuning
     */
    AX_REG_POWCTRL = 0xF08,  /* Power Control */
    AX_REG_REF = 0xF0D,      /* Reference */
    AX_REG_XTALOSC = 0xF10,  /* Crystal Oscillator Control */
    AX_REG_XTALAMPL = 0xF11, /* Crystal Oscillator Amplitude Control */

    _f00 = 0xF00,
    _f18 = 0xF18,
    _f1c = 0xF1c,
    _f21 = 0xF21,
    _f22 = 0xF22,
    _f23 = 0xF23,
    _f26 = 0xF26,
    _f34 = 0xF34,
    _f35 = 0xF35,
    _f44 = 0xF44,

    _f30 = 0xF30,
    _f31 = 0xF31,
    _f32 = 0xF32,
    _f33 = 0xF33,

    AX_REG_MODCFGP = 0xF5f,
}

impl Register {
    fn addr(self) -> u16 {
        self as u16
    }
}
