# vdb

Versatile hardware platform for LPWAN developments

A Sub 1GHZ Wireless Module for various Sub 1GHZ projects and applications

Hardware Specs
- AX5043 RF Transceiver
- STM32L4 ARM Cortex M4
- Standalone or RPI Hat modes
- USB 2.0 connectivity
- USART connectivity
- SPI connectivity
- USB bus / RPI/ battery powered

![Top](img/vdb-top.png)

![Bottom](img/vdb-bottom.png)
