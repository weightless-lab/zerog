#ifndef _PHY_H___
#define _PHY_H___

enum burst_type {
        SIB0,
        SIB1,
        DL_RA,
		UL_RA,
		DL_ALLOC,
		UL_ALLOC,
};

struct sib0 {
	uint16_t bs_id;
	uint16_t bsn_id;
	uint16_t sfn;
	uint16_t frame_duration:2;
	uint16_t version:1;
	uint16_t sib1_present:1;
	uint16_t sib_flag:1;
	uint16_t ns:2;
	uint16_t slice_duration:1;
	uint16_t even_ra_mcs:4;
	uint16_t odd_ra_mcs:4;
	uint16_t crc;
}__attribute__((__packed__));

struct sib1 {
	/* empty, because we don't support frequency hopping */
}__attribute__((__packed__));

struct dl_ra_flags {
	uint8_t dl_ra_start_ts_l:2;
	uint8_t rfu:5;
	uint8_t sib_flag:1;
}__attribute__((__packed__));


struct dl_ra_desc {
	uint8_t dl_ra_num_slots_0:4;
	uint8_t dl_ra_num_slots_1:4;
}__attribute__((__packed__));

struct dl_ra_info {
	struct dl_ra_desc dl_ra_desc;
	uint16_t dl_ra_dest_eid0_h;
	uint16_t dl_ra_dest_eid1_h;
}__attribute__((__packed__));

struct dl_ra_mcs {
	uint8_t dl_ra_mcs_0:4;
	uint8_t dl_ra_mcs_1:4;
}__attribute__((__packed__));

struct dl_ra {
	uint8_t sfn_l;
	struct dl_ra_flags dl_ra_flags;
	uint8_t dl_start_ts_h;
	uint8_t dl_ra_num;
	//followed by struct dl_ra_info dl_ra_info[dl_ra_num];
	//followed by struct dl_ra_mcs dl_ra_mcs[dl_ra_num];
}__attribute__((__packed__));

struct ul_ra_flags {
	uint8_t ul_ra_start_ts_l:2;
	uint8_t rfu:5;
	uint8_t sib_flag:1;
}__attribute__((__packed__));

struct ul_ra_desc {
	uint8_t ul_ra_num_slots:4;
	uint8_t ul_ra_mcs:4;
}__attribute__((__packed__));

struct ul_ra_info {
	struct ul_ra_desc ul_ra_desc;
	uint16_t ul_ra_dest_eid_h;
}__attribute__((__packed__));

struct ul_ra {
	uint8_t sfn_l;
	struct ul_ra_flags ul_ra_flags;
	uint8_t ul_start_ts_h;
	uint8_t ul_ra_num;
	//followed by struct ul_ra_info ul_ra_info[ul_ra_num];
}__attribute__((__packed__));

/* context used during transmission of DL */
struct tx_dl_ctxt {
	uint16_t warfcn;
	uint16_t bs_id;
	uint16_t bsn_id;
	uint16_t sfn;
	uint8_t frame_duration;
	uint8_t network_slice;
	uint8_t slice_duration;
	fec_scheme fs;
};

float complex *transmit_dl(struct tx_dl_ctxt *config, int *num_samples);

#endif /* not _PHY_H_ */

