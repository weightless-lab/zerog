/* first phy implementation of https://pro-bee-user-content-eu-west-1.s3.amazonaws.com/public/users/Integrators/929cb090-e779-401a-b06c-c629ff6b0fea/ap-cambridgestartuplimi/Weightless-P_v1.03.pdf */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <assert.h>
#include <stdbool.h>

#include "liquid.h"

#include "phy.h"

extern uint16_t crc16(uint8_t *data_p, uint16_t);

/* list of preambles defined in 2.2.7 */

/* FIXME reduce preambles when TCXO is implemented */
/* GSMK and FEC */
uint8_t preamble[] = {0xf2, 0x34, 0x21, 0xf6, 0xaf, 0xaa, 0xaa, 0xaa, 0xaf, 0x09, 0xde};
/* GMSK and no FEC */
//uint8_t preamble[] = {0xf2, 0x34, 0x21, 0xf6, 0xaf, 0x55, 0x55, 0x55, 0xf5, 0xbc, 0x6c};

#define PREAMBLE_SIZE sizeof(preamble) 

static void whitening_encode(uint16_t state, int data_len, uint8_t *data_in, uint8_t *data_out) {
    data_out[0] = data_in[0] ^ (state & 0xff);

    for (int j=1; j<data_len;j++){

        for (int i=0; i<8;i++) {
            state = (state >> 1) + (((state & 1) ^ ((state >> 5) & 1)) << 8);
        }
        data_out[j] = data_in[j] ^ (state & 0xff);
    }
}

static void interleaver2_encode(int data_len, uint8_t *data_in, uint8_t *data_out) {

    for (int i=0; i<data_len;i += 2) {

        data_out[i + 0] =
            (((data_in[i + 1] >> 0) & 1) << 7) + (((data_in[i + 1] >> 4) & 1) << 6) +
            (((data_in[i + 0] >> 0) & 1) << 5) + (((data_in[i + 0] >> 4) & 1) << 4) +
            (((data_in[i + 1] >> 1) & 1) << 3) + (((data_in[i + 1] >> 5) & 1) << 2) +
            (((data_in[i + 0] >> 1) & 1) << 1) + (((data_in[i + 0] >> 5) & 1) << 0);

        data_out[i + 1] =
            (((data_in[i + 1] >> 2) & 1) << 7) + (((data_in[i + 1] >> 6) & 1) << 6) +
            (((data_in[i + 0] >> 2) & 1) << 5) + (((data_in[i + 0] >> 6) & 1) << 4) +
            (((data_in[i + 1] >> 3) & 1) << 3) + (((data_in[i + 1] >> 7) & 1) << 2) +
            (((data_in[i + 0] >> 3) & 1) << 1) + (((data_in[i + 0] >> 7) & 1) << 0);
    }
}

uint8_t* encode_burst(enum burst_type burst_type, uint8_t *data_in, uint32_t data_in_len, uint8_t sfn_l, uint16_t warfcn, fec_scheme fs, uint32_t* data_out_len) {

    // compute the encoded message length
    uint32_t n_enc = fec_get_enc_msg_length(fs, data_in_len);

    uint8_t msg_enc[n_enc]; /* encodec message */
    uint8_t msg_interleaved[n_enc];
    uint8_t msg_whitened[n_enc];

    // generate fec object
    fec q = fec_create(fs, NULL);

    // encode message
    fec_encode(q, data_in_len, data_in, msg_enc);

    // interleave the data
    interleaver2_encode(n_enc, msg_enc, msg_interleaved);

    // perform whitening step
    uint16_t seed;

    if (burst_type == SIB0 || burst_type == SIB1) {
        seed = 0x1D3 ^ (warfcn & 0x1ff);
    } else {
        seed = 0x100 | sfn_l;
    }

    whitening_encode(seed, n_enc, msg_interleaved, msg_whitened);

    // destroy the fec object
    fec_destroy(q);

    uint8_t *res = malloc(n_enc + PREAMBLE_SIZE);
    memcpy(&res[0], preamble, PREAMBLE_SIZE);
    memcpy(&res[PREAMBLE_SIZE], msg_whitened, n_enc);

    *data_out_len = n_enc + PREAMBLE_SIZE;

    return res;
}

unsigned int gmsk_mod(uint8_t *data_buf, int num_bytes, float complex *samples) {

    uint32_t k=4;                   // filter samples/symbol
    uint32_t m=3;                   // filter delay (symbols)
    float BT=0.3f;                      // bandwidth-time product

    // derived values
    uint32_t num_data_symbols = num_bytes * 8;
    uint32_t num_symbols = num_data_symbols + 2*m;

    uint32_t M;
    uint8_t s[num_symbols];

    memset(s, 0, sizeof(s));
    liquid_unpack_bytes(data_buf, num_bytes, s, sizeof(s), &M);

    // create modulator
    gmskmod mod = gmskmod_create(k, m, BT);

    // modulate signal
    for (int i=0; i<num_symbols; i++) {
        gmskmod_modulate(mod, s[i], &samples[k*i]);
    }

    // destroy modem objects
    gmskmod_destroy(mod);

    return 0;
}

int encode_bpd(uint8_t *data_in, uint8_t data_len, uint8_t* bpd) {

    bpd[0] = data_len;

    memcpy(&bpd[1], data_in, data_len);

    uint32_t crc = crc16(bpd, data_len + 1);

    bpd[bpd[0] + 1] = crc >> 8;
    bpd[bpd[0] + 2] = crc & 0xff;

    return data_len + 3;
}

int encode_sib0(struct sib0 *sib0, uint8_t* data_out) {

    memset(data_out, 0, 10);

    data_out[0] = (sib0->bs_id >> 8) & 0xff;
    data_out[1] = sib0->bs_id & 0xff;

    data_out[2] = (sib0->bsn_id >> 8) & 0xff;
    data_out[3] = sib0->bsn_id & 0xff;

    data_out[4] = (sib0->sfn >> 8) & 0xff;
    data_out[5] = sib0->sfn & 0xff;

    data_out[6] |= ((sib0->frame_duration) & 0x3) << 6;

    uint32_t crc = crc16(data_out, 8);

    data_out[8] = crc >> 8;
    data_out[9] = crc & 0xff;

    return 0;
}

int encode_ul_ra(struct ul_ra *ul_ra, uint8_t* data_out) {
    data_out[0] = ul_ra->sfn_l;
    data_out[1] = 0x80;
    data_out[2] = 3;
    data_out[3] = 1;
    data_out[4] = 0xfb;
    data_out[5] = 0x7f;
    data_out[6] = 0xfe;

    return 0;
}

int encode_dl_ra(struct dl_ra *dl_ra, uint8_t* data_out, uint8_t i) {

    int start_ts = i;
    int sib_flag = 0;

    data_out[0] = dl_ra->sfn_l;

    data_out[1] = (start_ts & 0x3) << 6;
    data_out[1] |= (sib_flag & 1);

    data_out[2] = start_ts >> 2;
    data_out[3] = 1;

    data_out[4] = 0x11;

    data_out[5] = 0xff;
    data_out[6] = 0xff;

    data_out[7] = 0xaa;
    data_out[8] = 0xaa;

    data_out[9] = 0x00;

    return 0;
}

int encode_dl_alloc(uint8_t* data_out, uint8_t i) {
    if (i == 0) {
        data_out[0] = 0x6;
        data_out[1] = 0x11;
        data_out[2] = 0x12;
        data_out[3] = 0x13;
        data_out[4] = 0x0;
        data_out[5] = 0x1;
        data_out[6] = 0x2;
        data_out[7] = 0x3;
        data_out[8] = 0x4;
        data_out[9] = 0x5;
        data_out[10] = 0x6;
        data_out[11] = 0x7;
        data_out[12] = 0x8;
        data_out[13] = 0x9;
        data_out[14] = 0xa;
        data_out[15] = 0xb;
        data_out[16] = 0xc;
        data_out[17] = 0xd;
        data_out[18] = 0xe;
        data_out[19] = 0xf;
    } else {
        data_out[0] = 0xde;
        data_out[1] = 0xad;
        data_out[2] = 0xbe;
        data_out[3] = 0xef;
        data_out[4] = 0x10+i;
    }

    return 0;
}

float complex *transmit_dl(struct tx_dl_ctxt *config, int *num_samples2) {

    uint16_t warfcn = config->warfcn;
    fec_scheme fs = config->fs;

    struct sib0 sib0;
    memset(&sib0, 0, sizeof(sib0));

    sib0.bs_id = config->bs_id;
    sib0.bsn_id = config->bsn_id;
    sib0.sfn = config->sfn;
    sib0.frame_duration = config->frame_duration;

    struct dl_ra dl_ra;
    dl_ra.sfn_l = config->sfn & 0xff;

    struct ul_ra ul_ra;
    ul_ra.sfn_l = config->sfn & 0xff;

    uint8_t sfn_l = sib0.sfn & 0xff;

    uint32_t sib0_cc_len;
    uint32_t dl_ra_cc_len;
    uint32_t ul_ra_cc_len;
    uint32_t dl_alloc_cc_len;

    uint8_t *sib0_cc = NULL;
    uint8_t *dl_ra_cc = NULL;
    uint8_t *ul_ra_cc = NULL;
    uint8_t *dl_alloc_cc = NULL;


    uint8_t bpd[255];
    uint8_t sib0_encoded[10];
    uint8_t ul_ra_encoded[7];
    uint8_t dl_ra_encoded[10];
    uint8_t dl_alloc_encoded[20];

    encode_sib0(&sib0, sib0_encoded);
    sib0_cc = encode_burst(SIB0, sib0_encoded, sizeof(sib0_encoded), sfn_l, warfcn, fs, &sib0_cc_len);

    unsigned int frame_duration_in_tb = (2 << config->frame_duration) /(10*1e-6);

    unsigned int num_symbols = frame_duration_in_tb;
    unsigned int k = 4;
    unsigned int num_samples = num_symbols * k;
    unsigned int sibs_length = pow(2,config->network_slice)*5000;

    unsigned int dl_ra_length = pow(2,config->network_slice)*1875 * (config->slice_duration + 1);
    unsigned int ul_ra_length = pow(2,config->network_slice)*1875 * (config->slice_duration + 1);
    unsigned int dl_alloc_length = 2500;

    float complex *samples = (float complex *) malloc((num_samples) * sizeof(float complex));
    memset(samples, 0, num_samples * sizeof(float complex));

    unsigned int offset_sib0 = 0;
    gmsk_mod(sib0_cc, sib0_cc_len, &samples[offset_sib0]); 

    unsigned int offset_dl_ra = offset_sib0 + sibs_length*k;

    for (int i=0; i<4; i++) {
        encode_dl_ra(&dl_ra, dl_ra_encoded, i);
        uint8_t len = encode_bpd(dl_ra_encoded, sizeof(dl_ra_encoded), bpd);
        dl_ra_cc = encode_burst(DL_RA, bpd, len, sfn_l, warfcn, fs, &dl_ra_cc_len);
        gmsk_mod(dl_ra_cc, dl_ra_cc_len, &samples[offset_dl_ra + i*dl_ra_length*k]);
    }

    unsigned int offset_ul_ra = offset_dl_ra + dl_ra_length*k*4;

    for (int i=0; i<8; i++) {
        encode_ul_ra(&ul_ra, ul_ra_encoded);
        uint8_t len = encode_bpd(ul_ra_encoded, sizeof(ul_ra_encoded), bpd);
        ul_ra_cc = encode_burst(UL_RA, bpd, len, sfn_l, warfcn, fs, &ul_ra_cc_len);
        gmsk_mod(ul_ra_cc, ul_ra_cc_len, &samples[offset_ul_ra + i*ul_ra_length*k]);
    }

    unsigned int offset_dl_alloc = offset_ul_ra + ul_ra_length*k*8;

    for (int i=0; i<16; i++) {
        encode_dl_alloc(dl_alloc_encoded, i);
        uint8_t len = encode_bpd(dl_alloc_encoded, sizeof(dl_alloc_encoded), bpd);
        dl_alloc_cc = encode_burst(DL_ALLOC, bpd, len, sfn_l, warfcn, fs, &dl_alloc_cc_len);
        gmsk_mod(dl_alloc_cc, dl_alloc_cc_len, &samples[offset_dl_alloc + i*dl_alloc_length*k]);
    }

    free(sib0_cc);
    free(dl_ra_cc);
    free(ul_ra_cc);

    config->sfn ++;
    *num_samples2 = offset_dl_alloc + dl_alloc_length*k*16;
    return samples;
}
