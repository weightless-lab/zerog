#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <signal.h>

#include "uhd.h"
#include "liquid.h"
#include "phy.h"

struct complex_sample {
    float i;
    float q;
};

struct config {
	uint16_t warfcn;
	uint8_t frame_duration;
	uint8_t slice_duration;
	uint8_t network_slice;
};

static bool stop_signal_called = false;

static void sigint_handler(int code){
	(void)code;
	stop_signal_called = true;
}

static void *thread_tx(void *ptr){

	struct config *config = (struct config *) ptr;

	printf("warfcn = %d\n", config->warfcn);
	printf("frame duration = %d\n", config->frame_duration);
	printf("network slice = %d\n", config->network_slice);
	printf("slice duration = %d\n", config->slice_duration);

	double freq = config->warfcn*1e5;

	double rate = 400*1e3;
	double gain = 30.0;
	char* device_args = NULL;
	size_t channel = 0;
	size_t samps_per_buff;
	const void** buffs_ptr = NULL;

	device_args = strdup("");

	// Create USRP
	uhd_usrp_handle usrp;
	printf("Creating USRP with args \"%s\"...\n", device_args);
	uhd_usrp_make(&usrp, device_args);

	// Create TX streamer
	uhd_tx_streamer_handle tx_streamer;
	uhd_tx_streamer_make(&tx_streamer);

	// Create other necessary structs
	uhd_tune_request_t tune_request = {
		.target_freq = freq,
		.rf_freq_policy = UHD_TUNE_REQUEST_POLICY_AUTO,
		.dsp_freq_policy = UHD_TUNE_REQUEST_POLICY_AUTO
	};
	uhd_tune_result_t tune_result;

	uhd_stream_args_t stream_args = {
		.cpu_format = "fc32",
		.otw_format = "sc16",
		.args = "",
		.channel_list = &channel,
		.n_channels = 1
	};

	// Set rate
	printf("Setting TX Rate: %f...\n", rate);
	uhd_usrp_set_tx_rate(usrp, rate, channel);

	// See what rate actually is
	uhd_usrp_get_tx_rate(usrp, channel, &rate);
	printf("Actual TX Rate: %f...\n\n", rate);

	// Set gain
	printf("Setting TX Gain: %f db...\n", gain);
	uhd_usrp_set_tx_gain(usrp, gain, 0, "");

	// See what gain actually is
	uhd_usrp_get_tx_gain(usrp, channel, "", &gain);
	printf("Actual TX Gain: %f...\n", gain);

	// Set frequency
	printf("Setting TX frequency: %f MHz...\n", freq / 1e6);
	uhd_usrp_set_tx_freq(usrp, &tune_request, channel, &tune_result);

	// See what frequency actually is
	uhd_usrp_get_tx_freq(usrp, channel, &freq);
	printf("Actual TX frequency: %f MHz...\n", freq / 1e6);

	// Set up streamer
	stream_args.channel_list = &channel;
	uhd_usrp_get_tx_stream(usrp, &stream_args, tx_streamer);

	// Set up buffer
	uhd_tx_streamer_max_num_samps(tx_streamer, &samps_per_buff);
	printf("Buffer size in samples: %zu\n", samps_per_buff);

	struct tx_dl_ctxt tx_dl_ctxt;

	tx_dl_ctxt.warfcn = config->warfcn;
	tx_dl_ctxt.bs_id  = 0xff80;
	tx_dl_ctxt.bsn_id = 0xff00;
	tx_dl_ctxt.sfn = 7;

	tx_dl_ctxt.fs     = LIQUID_FEC_CONV_V27;

	tx_dl_ctxt.frame_duration = config->frame_duration; /* 0,1,2,3 means 2,4,8,16 in seconds */
	tx_dl_ctxt.network_slice  = 0; /* 0,1,2,3 means 1,2,4,8 */
	tx_dl_ctxt.slice_duration = 0; /* 0,1 means 75ms/150ms for DL and 150ms/300ms for UL */

	// Ctrl+C will exit loop
	signal(SIGINT, &sigint_handler);
	fprintf(stderr, "Press Ctrl+C to stop streaming...\n");

	// Actual streaming
	size_t num_samps_sent = 0;

	int64_t full_secs = 0;
	double frac_secs = 0;

	uhd_usrp_set_time_now(usrp, full_secs, frac_secs, 0);

	int tti = 2;
	
	// Create TX metadata
	uhd_tx_metadata_handle md;

	while (!stop_signal_called) {

		int num_samples;
		complex float *samples = transmit_dl(&tx_dl_ctxt, &num_samples);

		struct complex_sample *tx_samples = NULL;
		unsigned int num_samples_tx;

		num_samples_tx = num_samples;

		//Allocate tx samples
		tx_samples = (struct complex_sample *) malloc(num_samples_tx * sizeof(struct complex_sample));
		if (tx_samples == NULL){
			perror("malloc");
			return NULL;
		}

		for (int i=0; i<num_samples_tx; i++) {
			tx_samples[i].i = crealf(samples[i]);
			tx_samples[i].q = cimagf(samples[i]);
		}

		buffs_ptr = (const void**)&tx_samples;

		uhd_tx_metadata_make(&md, true,  tti, 0.0, false, true);
		tti += (2 << tx_dl_ctxt.frame_duration);

		uhd_tx_streamer_send(tx_streamer, buffs_ptr, num_samples_tx, &md, 20, &num_samps_sent);

		free(tx_samples);
		free(samples);
	}

	return 0;
}


int main(int argc, char *argv[]) {
	int opt;

	struct config config;

	/* set default values */
	config.warfcn = 8700;
	config.frame_duration = 0;
	config.network_slice = 0;
	config.slice_duration = 0;

	printf("wbs simulator:\n");

	while ((opt = getopt(argc, argv, "w:f:n:s:")) != -1) {
		switch (opt) {
			case 'w':
				config.warfcn = atoi(optarg);
				break;
			case 'f':
				config.frame_duration = atoi(optarg);
				break;
			case 'n':
				config.network_slice = atoi(optarg);
				break;
			case 's':
				config.slice_duration = atoi(optarg);
				break;
			default:
				fprintf(stderr, "Usage: %s [-w warfcn] [-f frame_duration] [-n network_slice] [-s slice_duration]\n", argv[0]);
				exit(EXIT_FAILURE);
		}
	}

	pthread_t thread;

	pthread_create(&thread, NULL, *thread_tx, (void *)&config);

	pthread_join(thread,NULL);

	return 0;
}
