#![deny(warnings)]
#![allow(non_camel_case_types)]

use rtt_target::rprintln;

use crate::app::*;
use heapless::Vec;
use rtic::Mutex;
use stm32l4xx_hal::lptimer::Event::{AutoReloadMatch, CompareMatch};
use stm32l4xx_hal::timer::Event;

use phy::{crc16, decode};

use crate::bb;

const LP_XO: u16 = 32_768; /* frequency of MCU LP oscillator, commonly 32Khz  */
const LP_DIV: u16 = 16; /* LP Timer Prescaler */
const RF_XO: f32 = 48_000_000.0; /* frequency of TRX oscillator */

const TICKS_PER_SEC: u16 = LP_XO / LP_DIV;

const FRAME_IND_OFFSET: f32 = 0.01; /* trigger high layers activity 10ms ahead of new frame */
const TRX_INIT_DURATION: f32 = 0.005; /* init and configure RF transceiver 5ms second before reception */
const PREAMBLE_DURATION: f32 = 40.0 / 100_000.0; /* 16 + 24 bits, includes preamble and syncword */

const MAX_BUF_SIZ: usize = 256;
pub const MAX_ALLOC_NUM: usize = 16;
const MAX_RF_JOBS_NUM: usize = 16;
const MAX_FREQ_PER_SCAN: usize = 32;

#[derive(PartialEq, Debug)]
pub enum FromRrm {
    CPHY_START_REQ(CphyStartReq),
    CPHY_STOP_REQ(CphyStopReq),

    CPHY_RSSI_SCAN_REQ(CphyRssiScanReq),
    CPHY_CELL_SEARCH_REQ(CphyCellSearchReq),
    CPHY_CELL_SELECT_REQ(CphyCellSelectReq),

    CPHY_CONFIG_REQ(CphyConfigReq),
    CPHY_MEASUREMENT_REQ(CphyMeasurementReq),
}

#[derive(PartialEq, Debug)]
pub enum ToRrm {
    CPHY_START_CNF(CphyStartCnf),
    CPHY_STOP_CNF(CphyStopCnf),

    CPHY_RSSI_SCAN_CNF(CphyRssiScanCnf),
    CPHY_CELL_SEARCH_CNF(CphyCellSearchCnf),
    CPHY_CELL_SELECT_CNF(CphyCellSearchCnf),

    CPHY_CONFIG_CNF(CphyConfigCnf),
    CPHY_MEASUREMENT_IND(CphyMeasurementInd),
}

#[derive(PartialEq, Debug)]
pub enum FromBb {
    PHY_RECEIVE_REQ(PhyReceiveReq),
    PHY_TRANSMIT_REQ(PhyTransmitReq),
}

#[derive(PartialEq, Debug)]
pub enum ToBb {
    PHY_FRAME_IND(PhyFrameInd),
    PHY_DATA_IND(PhyDataInd),
}

#[derive(PartialEq, Debug)]
pub struct CphyMeasurementInd {
    pub warfcn: u16,
    pub rssi: u8,
    pub slice_idx: u8,
}

#[derive(PartialEq, Debug)]
pub struct CphyStartReq {}

#[derive(PartialEq, Debug)]
pub struct CphyStartCnf {}

#[derive(PartialEq, Debug)]
pub struct CphyStopReq {}

#[derive(PartialEq, Debug)]
pub struct CphyStopCnf {}

#[derive(PartialEq, Debug, Default)]
pub struct CphyRssiScanReq {
    pub nb_freq: u16,
    pub warfcn_start: u16,
}

#[derive(PartialEq, Debug)]
pub struct Result {
    pub warfcn: u16,
    pub rssi: u8,
}

#[derive(PartialEq, Debug, Default)]
pub struct CphyRssiScanCnf {
    pub result: Vec<Result, MAX_FREQ_PER_SCAN>,
}

#[derive(PartialEq, Debug)]
pub struct CphyCellSearchReq {
    pub warfcn: u16,
}

#[derive(PartialEq, Debug, Default)]
pub struct CellInfo {
    pub bsn_id: u16,
    pub bs_id: u16,
    pub sfn: u16,
    pub frame_duration: u8,
    pub ns: u8,
    pub slice_duration: u8,
    pub even_ra_mcs: u8,
    pub odd_ra_mcs: u8,
}

#[derive(PartialEq, Debug)]
pub struct CphyCellSearchCnf {
    pub rssi: u8,
    pub cell_info: CellInfo,
}

#[derive(PartialEq, Debug)]
pub enum MeasurementType {
    Serving,
    IntraFreq,
    InterFreq,
}

impl Default for MeasurementType {
    fn default() -> MeasurementType {
        MeasurementType::Serving
    }
}

#[derive(PartialEq, Debug)]
pub struct CphyMeasurementReq {
    pub r#type: MeasurementType,
}

#[derive(PartialEq, Debug, Default)]
pub struct CphyConfigReq {
    pub warfcn: u16,
    pub bs_id: u16,
    pub frame_duration: u8,
    pub ns: u8,
    pub slice_duration: u8,
}

#[derive(PartialEq, Debug)]
pub struct CphyConfigCnf {}

#[derive(PartialEq, Debug)]
pub struct RxData {
    pub alloc_type: AllocType,
    pub data_buf: Vec<u8, MAX_BUF_SIZ>,
}

#[derive(PartialEq, Debug)]
pub struct PhyFrameInd {
    pub sfn: u16,
}

#[derive(PartialEq, Debug)]
pub struct PhyDataInd {
    pub sfn: u16,
    pub data: RxData,
}

#[derive(PartialEq, Debug, Copy, Clone)]
pub struct TsDesc {
    pub start_ts: u16,
    pub nb_ts: u8,
    pub channel_idx: Option<u8>,
}

#[derive(PartialEq, Debug, Clone)]
pub enum AllocType {
    None,
    SIB(u8),
    DL_RA(u8),
    UL_RA(u8),
    DL_ALLOC((TsDesc, bb::EIDType)),
    UL_ALLOC(TsDesc),
}

#[derive(PartialEq, Debug, Default)]
pub struct DlAlloc {
    pub alloc_type: AllocType,
    pub mcs: Option<u8>,
}

impl Default for AllocType {
    fn default() -> AllocType {
        AllocType::None
    }
}

#[derive(PartialEq, Debug, Default)]
pub struct PhyReceiveReq {
    pub sfn: u16,
    pub alloc: Vec<DlAlloc, MAX_ALLOC_NUM>,
}

#[derive(PartialEq, Debug, Default)]
pub struct UlAlloc {
    pub alloc_type: AllocType,
    pub mcs: u8,
    pub data: Vec<u8, MAX_BUF_SIZ>,
}

#[derive(PartialEq, Debug, Default)]
pub struct PhyTransmitReq {
    pub sfn: u16,
    pub alloc: Vec<UlAlloc, MAX_ALLOC_NUM>,
}

#[derive(PartialEq, Debug)]
pub struct CphyCellSelectReq {
    pub warfcn: u16,
    pub timing: u32,
}

#[derive(PartialEq, Debug)]
pub enum L1Event {
    Init,
    Timer1,
    Timer2,
    TX,
    CphyCellSearchCnf(CphyCellSearchCnf),
    TRX_IT,
    Rrm(FromRrm),
    Bb(FromBb),
}

#[derive(Debug, PartialEq)]
enum State {
    Null,
    NoCell,
    CellSearch,
    CellCamped,
}

impl Default for State {
    fn default() -> State {
        State::Null
    }
}

#[derive(Default, Debug, Clone)]
struct RfJob {
    alloc_type: AllocType,
    sfn: u16,
    rf_start: u16,
    rf_stop: u16,
    expected_sfd: u16,
}

#[derive(Debug, PartialEq)]
enum SchedulerState {
    Null,
    FrameInd,
    RfJobs,
}

impl Default for SchedulerState {
    fn default() -> SchedulerState {
        SchedulerState::Null
    }
}

enum TrxLowPowerState {
    Null,
    DeepSleep,
    Standby,
}

impl Default for TrxLowPowerState {
    fn default() -> TrxLowPowerState {
        TrxLowPowerState::Null
    }
}

#[derive(Default)]
pub struct L1 {
    state: State,

    /* cell attributes */
    sfn: Option<u16>,
    warfcn: u16,
    bs_id: u16,
    frame_duration: u16,
    ns: u8,
    slice_duration: u8,

    /* scheduled jobs */
    scheduler_state: SchedulerState,
    frame_start: u16,
    rf_jobs: Vec<RfJob, MAX_RF_JOBS_NUM>,

    /* timing info */
    sfd_ax: u32,
    timing_drift: i16,

    trx_low_power_state: TrxLowPowerState,
    tx_alloc: Vec<u8, MAX_BUF_SIZ>, /* TODO it should be a list of TX RF jobs */

    /* mcs related parameters */
    is_fec: bool,
    is_gmsk: bool,
}

impl L1 {
    pub fn new() -> L1 {
        Default::default()
    }
}

fn start_rf_receiver(ax: &mut AxDriver, warfcn: u16, is_fec: bool, is_gmsk: bool) {
    let preamble;
    let syncword;

    if is_gmsk {
        if is_fec {
            preamble = &[0x55, 0x55];
            syncword = &[0xf5, 0x90, 0x7b];
        } else {
            preamble = &[0xaa, 0xaa];
            syncword = &[0xaf, 0x3d, 0x36];
        }
    } else {
        panic!("only gmsk is supported");
    }

    ax.reset().unwrap();
    ax.init(warfcn).unwrap();
    ax.perf_registers().unwrap();
    ax.pattern_matching(preamble, syncword).unwrap();
    ax.packet_controller().unwrap();
    ax.rx_parameters().unwrap();
    ax.enable_rx().unwrap();
}

fn start_rf_transmitter(ax: &mut AxDriver, warfcn: u16) {
    ax.init(warfcn).unwrap();
    ax.perf_registers().unwrap();
    ax.tx_parameters().unwrap();
    ax.enable_tx().unwrap();
}

fn decode_sib0(data: &[u8]) -> Option<CellInfo> {
    let mut res = None;

    /* SIB0 length is 10 bytes */
    if data.len() >= 10 {
        let crc;
        unsafe {
            crc = crc16(data.as_ptr(), 8);
        }

        if data[8] == (crc >> 8) as u8 && data[9] == (crc & 0xff) as u8 {
            let mut sib0: CellInfo = Default::default();

            sib0.bs_id = ((data[0] as u16) << 8) + data[1] as u16;
            sib0.bsn_id = ((data[2] as u16) << 8) + data[3] as u16;
            sib0.sfn = ((data[4] as u16) << 8) + data[5] as u16;

            sib0.frame_duration = (data[6] >> 6) & 0x3;

            if (data[6] & 0x20) == 0x20 {
                panic!("only version 0 is supported");
            }

            if (data[6] & 0x10) == 0x10 {
                rprintln!("SIB1 not supported yet");
            }

            if (data[6] & 0x08) == 0x08 {
                rprintln!("SIB FLAG not supported yet");
            }

            sib0.ns = (data[6] >> 1) & 0x3;
            sib0.slice_duration = data[6] & 0x1;

            sib0.even_ra_mcs = (data[7] >> 4) & 0xf;
            sib0.odd_ra_mcs = data[7] & 0xf;

            res = Some(sib0);
        }
    }

    res
}

fn trx_handler(mut cx: l1_task::Resources) {
    let ctxt = &mut cx.l1;
    let ax = &mut cx.ax;
    let lptim = &mut cx.lptim;

    /* TODO check status to confirm fifo not empty interrupt */
    while ax.is_fifo_not_empty().unwrap() {
        if let Ok((mut symbols, ts)) = ax.read_fifo() {
            if let Some(sfd_ax) = ts {
                ctxt.sfd_ax = sfd_ax;
            }

            if !symbols.is_empty() {
                let seed = if let Some(sfn) = ctxt.sfn {
                    if let AllocType::SIB(_) = ctxt.rf_jobs[0].alloc_type {
                        0x1d3 ^ (ctxt.warfcn & 0x1ff)
                    } else {
                        0x100 ^ (sfn & 0xff)
                    }
                } else {
                    0x1d3 ^ (ctxt.warfcn & 0x1ff)
                };

                match decode(ctxt.is_fec, seed, &mut symbols) {
                    Ok(pdu) => {
                        if ctxt.state == State::CellSearch {
                            /* report back sib0 and rssi to RRM */
                            if let Some(cell_info) = decode_sib0(&pdu) {
                                l1_task::spawn(L1Event::CphyCellSearchCnf(CphyCellSearchCnf {
                                    /* FIXME : rssi not supported for now */
                                    rssi: 0,
                                    cell_info,
                                }))
                                .ok();
                            }
                        } else if ctxt.state == State::CellCamped {
                            /* get SIB0 timing and information */
                            if ctxt.sfn.is_none() {
                                if let Some(cell_info) = decode_sib0(&pdu) {
                                    ctxt.sfn = Some(cell_info.sfn);
                                    if let Ok(curtime) = ax.get_timer() {
                                        ax.enter_deepsleep().unwrap();
                                        lptim.lock(|lptim| {
                                            /* dirty way used to init the LP counter as there is
                                             * no dedicated way for now in the driver */
                                            lptim.listen(CompareMatch);
                                            lptim.listen(AutoReloadMatch);

                                            ctxt.scheduler_state = SchedulerState::FrameInd;

                                            /* Compute the next frame start from SFD */
                                            ctxt.frame_start = ((-((curtime - ctxt.sfd_ax) as f32)
                                                / (RF_XO / 16.0)
                                                - PREAMBLE_DURATION
                                                - FRAME_IND_OFFSET
                                                + ctxt.frame_duration as f32)
                                                * TICKS_PER_SEC as f32)
                                                as u16;

                                            lptim.set_compare_match(ctxt.frame_start);
                                            lptim.set_autoreload(0xffff);
                                        });
                                    }
                                }
                            } else {
                                let mut crc_ok = false;
                                /* TODO add CRC check before next delivery */

                                match ctxt.rf_jobs[0].alloc_type {
                                    AllocType::UL_RA(_)
                                    | AllocType::DL_RA(_)
                                    | AllocType::DL_ALLOC(_) => {
                                        let len = (pdu[0] + 1) as usize;
                                        if len + 2 <= pdu.len() {
                                            let crc;
                                            unsafe {
                                                crc = crc16(pdu.as_ptr(), len as u16);
                                            }

                                            if crc == ((pdu[len] as u16) << 8) + pdu[len + 1] as u16
                                            {
                                                crc_ok = true;
                                            }
                                        }
                                    }
                                    _ => {}
                                }

                                if crc_ok {
                                    lptim.lock(|lptim| {
                                        let now = lptim.get_counter();

                                        if let Ok(curtime) = ax.get_timer() {
                                            let sfd_mcu = now as f32
                                                - ((curtime - ctxt.sfd_ax) as f32) / (RF_XO / 16.0)
                                                    * TICKS_PER_SEC as f32;

                                            let drift = ctxt.rf_jobs[0].expected_sfd as i16
                                                - sfd_mcu as i16;
                                            if drift.abs() > 5 {
                                                rprintln!(
                                                    "debug drift {} {} {}",
                                                    sfd_mcu,
                                                    ctxt.rf_jobs[0].expected_sfd,
                                                    drift.abs()
                                                );
                                            } else {
                                                ctxt.timing_drift = drift;
                                                /* FIXME handle CRC KO */
                                                let msg = PhyDataInd {
                                                    sfn: ctxt.rf_jobs[0].sfn,
                                                    data: RxData {
                                                        alloc_type: ctxt.rf_jobs[0]
                                                            .alloc_type
                                                            .clone(),
                                                        data_buf: pdu,
                                                    },
                                                };
                                                L1::send_to_bb(ToBb::PHY_DATA_IND(msg));
                                            }
                                        }
                                    });
                                }
                            }
                        }
                    }
                    Err(err) => {
                        /* log only decoding errors in framed synced cell Camped state */
                        if ctxt.state == State::CellCamped && ctxt.sfn.is_some() {
                            rprintln!("Decoding failure : {}", err);
                        }
                    }
                }
            }
        }
    }
}

pub fn l1_entry(mut cx: l1_task::Resources, event: L1Event) {
    let ctxt = &mut cx.l1;
    let ax = &mut cx.ax;
    let lptim = &mut cx.lptim;
    let tim2 = &mut cx.tim2;

    match event {
        L1Event::Bb(_) | L1Event::Rrm(_) => {
            rprintln!("l1_entry {:x?}", event);
        }
        _ => {}
    }

    match &ctxt.state {
        State::Null => match event {
            L1Event::Init => {
                ctxt.is_fec = true;
                ctxt.is_gmsk = true;
            }
            L1Event::Rrm(FromRrm::CPHY_START_REQ(_)) => {
                ctxt.state = State::NoCell;
                L1::send_to_rrm(ToRrm::CPHY_START_CNF(CphyStartCnf {}));
            }
            _ => {
                unreachable!();
            }
        },
        State::NoCell => match event {
            L1Event::Rrm(FromRrm::CPHY_RSSI_SCAN_REQ(ref req)) => {
                /* TODO not handled yet */
                let mut cnf: CphyRssiScanCnf = Default::default();
                cnf.result
                    .push(Result {
                        warfcn: req.warfcn_start,
                        rssi: 80,
                    })
                    .ok();
                L1::send_to_rrm(ToRrm::CPHY_RSSI_SCAN_CNF(cnf));
            }
            L1Event::Rrm(FromRrm::CPHY_CELL_SEARCH_REQ(ref req)) => {
                ctxt.state = State::CellSearch;
                ctxt.warfcn = req.warfcn;
                /* TODO set a guard timer to avoid long receive */
                start_rf_receiver(ax, ctxt.warfcn, ctxt.is_fec, ctxt.is_gmsk);
            }
            _ => {
                unreachable!();
            }
        },
        State::CellSearch => match event {
            L1Event::CphyCellSearchCnf(val) => {
                L1::send_to_rrm(ToRrm::CPHY_CELL_SEARCH_CNF(val));
                ax.enter_deepsleep().unwrap();
            }
            L1Event::Rrm(FromRrm::CPHY_CONFIG_REQ(ref req)) => {
                ctxt.state = State::CellCamped;
                ctxt.warfcn = req.warfcn;
                ctxt.bs_id = req.bs_id;
                ctxt.frame_duration = (2 << req.frame_duration) as u16;
                ctxt.ns = req.ns;
                ctxt.slice_duration = req.slice_duration;

                L1::send_to_rrm(ToRrm::CPHY_CONFIG_CNF(CphyConfigCnf {}));

                start_rf_receiver(ax, ctxt.warfcn, ctxt.is_fec, ctxt.is_gmsk);
            }
            L1Event::TRX_IT => {
                trx_handler(cx);
            }
            _ => {
                unreachable!();
            }
        },
        State::CellCamped => match event {
            L1Event::Bb(FromBb::PHY_TRANSMIT_REQ(ref req)) => {
                if ctxt.tx_alloc.is_empty() {
                    ctxt.tx_alloc = req.alloc[0].data.clone();
                    tim2.lock(|tim| {
                        /* TODO compute properly ARR */
                        tim.reset();
                        tim.clear_update_interrupt_flag();
                        tim.listen(Event::TimeOut);
                    });
                } else {
                    todo!();
                }
            }
            L1Event::Bb(FromBb::PHY_RECEIVE_REQ(ref req)) => {
                let mut is_ul_ra = false;

                for alloc in req.alloc.iter() {
                    let offset;
                    let duration;

                    /* TODO take into account other parameters such as ns and slice duration */

                    match &alloc.alloc_type {
                        AllocType::SIB(idx) => {
                            duration = 0.025;
                            offset = 0.0 + (*idx as f32) * duration;
                        }
                        AllocType::DL_RA(idx) => {
                            duration = 0.075 / 4.0;
                            offset = 0.050 + (*idx as f32) * duration;
                        }
                        AllocType::UL_RA(idx) => {
                            is_ul_ra = true;
                            duration = 0.150 / 8.0;
                            offset = 0.050 + 0.075 + (*idx as f32) * duration;
                        }
                        AllocType::DL_ALLOC(desc) => {
                            duration = 0.025 * (desc.0.nb_ts as f32);
                            offset = 0.050 + 0.075 + 0.150 + (desc.0.start_ts as f32) * 0.025;
                        }
                        _ => {
                            unreachable!();
                        }
                    }

                    let mut job: RfJob = Default::default();

                    job.sfn = req.sfn;
                    job.alloc_type = alloc.alloc_type.clone();

                    job.rf_start = ctxt.frame_start
                        + ((FRAME_IND_OFFSET - TRX_INIT_DURATION + offset) * (TICKS_PER_SEC as f32))
                            as u16;

                    job.expected_sfd = job.rf_start
                        + ((TRX_INIT_DURATION + PREAMBLE_DURATION) * (TICKS_PER_SEC as f32)) as u16;

                    job.rf_stop = job.expected_sfd + (duration * (TICKS_PER_SEC as f32)) as u16;

                    ctxt.rf_jobs.push(job).unwrap();
                }

                /* schedule the first job */
                if ctxt.scheduler_state == SchedulerState::FrameInd && !ctxt.rf_jobs.is_empty() {
                    ctxt.trx_low_power_state = if is_ul_ra {
                        TrxLowPowerState::Standby
                    } else {
                        TrxLowPowerState::DeepSleep
                    };

                    ctxt.scheduler_state = SchedulerState::RfJobs;
                    lptim.lock(|lptim| {
                        lptim.set_compare_match(ctxt.rf_jobs[0].rf_start);
                        lptim.set_autoreload(ctxt.rf_jobs[0].rf_stop);
                    });
                }
            }
            L1Event::Timer1 => match ctxt.scheduler_state {
                SchedulerState::FrameInd => {
                    if let Some(sfn) = ctxt.sfn.as_mut() {
                        *sfn += 1;
                        L1::send_to_bb(ToBb::PHY_FRAME_IND(PhyFrameInd { sfn: *sfn }));
                    }
                }
                SchedulerState::RfJobs => {
                    start_rf_receiver(ax, ctxt.warfcn, ctxt.is_fec, ctxt.is_gmsk);
                }
                _ => {
                    unreachable!();
                }
            },
            L1Event::Timer2 => {
                if ctxt.scheduler_state == SchedulerState::RfJobs {
                    ctxt.frame_start -= ctxt.rf_jobs[0].rf_stop;

                    lptim.lock(|lptim| {
                        if ctxt.rf_jobs.len() > 1 {
                            for i in 1..ctxt.rf_jobs.len() {
                                ctxt.rf_jobs[i].rf_start -= ctxt.rf_jobs[0].rf_stop;
                                ctxt.rf_jobs[i].rf_stop -= ctxt.rf_jobs[0].rf_stop;
                                ctxt.rf_jobs[i].expected_sfd -= ctxt.rf_jobs[0].rf_stop;
                            }

                            let rf_jobs: Vec<RfJob, MAX_RF_JOBS_NUM> =
                                Vec::from_slice(&ctxt.rf_jobs[1..]).unwrap();
                            ctxt.rf_jobs = rf_jobs;

                            lptim.set_compare_match(ctxt.rf_jobs[0].rf_start);
                            lptim.set_autoreload(ctxt.rf_jobs[0].rf_stop);
                        } else {
                            ctxt.scheduler_state = SchedulerState::FrameInd;
                            ctxt.frame_start += TICKS_PER_SEC * ctxt.frame_duration;
                            ctxt.frame_start =
                                ((ctxt.frame_start as i32) - (ctxt.timing_drift as i32)) as u16;
                            lptim.set_compare_match(ctxt.frame_start);
                            lptim.set_autoreload(0xffff);
                            ctxt.rf_jobs.pop();
                        }
                    });
                }

                match ctxt.trx_low_power_state {
                    TrxLowPowerState::DeepSleep => {
                        ax.enter_deepsleep().unwrap();
                    }
                    TrxLowPowerState::Standby => {
                        ax.enter_standby().unwrap();
                    }
                    _ => {
                        unreachable!();
                    }
                }
            }
            L1Event::TRX_IT => {
                trx_handler(cx);
            }
            L1Event::TX => {
                if !ctxt.tx_alloc.is_empty() {
                    start_rf_transmitter(ax, ctxt.warfcn);

                    /* TODO Add preamble and sync word */

                    let mut bytes: Vec<u8, MAX_BUF_SIZ> =
                        Vec::from_slice(&[0xe1, ctxt.tx_alloc.len() as u8 + 1, 0x3b]).unwrap();
                    bytes.extend_from_slice(&ctxt.tx_alloc).unwrap();

                    ax.tx(&bytes).unwrap();

                    ctxt.tx_alloc.clear();
                }
            }
            _ => {
                unreachable!();
            }
        },
    }
}
