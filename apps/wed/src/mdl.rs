use core::fmt::Write;
use heapless::{String, Vec};
use rtt_target::rprintln;

use stm32l4xx_hal::{serial, stm32::USART1};

use crate::rrm::{FromMdl, ToMdl};
use crate::{ll, rrm};

const MAX_BUF_SIZ: usize = 256;
const MAX_STR_SIZ: usize = 8;

#[derive(PartialEq, Debug)]
enum MdlState {
    POWEROFF,
    POWERING,
    DEPOWERING,
    DEREGISTERED,
    DEREGISTERING,
    REGISTERING,
    REGISTERED,
}

#[derive(Default)]
pub struct Mdl {
    state: MdlState,
    atci: Atci,
    serial_number: String<MAX_STR_SIZ>,
    model: String<MAX_STR_SIZ>,
    manufacturer: String<MAX_STR_SIZ>,
    revision: String<MAX_STR_SIZ>,
}

impl Mdl {
    pub fn new() -> Mdl {
        Default::default()
    }
}

#[derive(Default)]
struct Atci {
    buf: String<MAX_BUF_SIZ>,
    //echo: bool,
}

impl Default for MdlState {
    fn default() -> MdlState {
        MdlState::POWEROFF
    }
}

#[derive(PartialEq, Debug)]
pub enum MdlEvent {
    Init,
    Serial(char),
    Rrm(rrm::ToMdl),
    Ll(ll::ToMdl),

    Start,
    Stop,
    Register,
    Deregister,
}

#[derive(PartialEq)]
enum CommandType {
    ACT, /* AT action command (ATCMD)		    */
    SET, /* AT set parameter command (ATCMD=)	*/
    GET, /* AT read parameter command (ATCMD?)	*/
    TST, /* AT test parameter command (ATCMD=?)	*/
}

struct AtCommand<'a> {
    pub command: &'a str,
    pub r#type: CommandType,
    pub fun: fn(command: &str, mdl: &Mdl) -> Result<ParsingSuccess, ParsingFailure>,
}

enum ParsingSuccess {
    Response(String<MAX_BUF_SIZ>),
    Event(MdlEvent),
}

#[derive(Debug)]
enum ParsingFailure {
    TrailingChars,
    UnexpectedParameter,
    UnexpectedValue,
    ParsingError,
}

const AT_COMMANDS: &[AtCommand] = &[
    /* List all available AT commands */
    AtCommand {
        command: "+CLAC",
        r#type: CommandType::ACT,
        fun: parse_clac_act,
    },
    /* Request Product Serial Number identification */
    AtCommand {
        command: "+CGSN",
        r#type: CommandType::ACT,
        fun: parse_cgsn_act,
    },
    /* Request manufacturer identification */
    AtCommand {
        command: "+CGMI",
        r#type: CommandType::ACT,
        fun: parse_cgmi_act,
    },
    /* Request model identification */
    AtCommand {
        command: "+CGMM",
        r#type: CommandType::ACT,
        fun: parse_cgmm_act,
    },
    /* Request revision identification */
    AtCommand {
        command: "+CGMR",
        r#type: CommandType::ACT,
        fun: parse_cgmr_act,
    },
    /* Set phone functionality */
    AtCommand {
        command: "+CFUN=",
        r#type: CommandType::SET,
        fun: parse_cfun_set,
    },
    AtCommand {
        command: "+CFUN?",
        r#type: CommandType::GET,
        fun: parse_cfun_get,
    },
    AtCommand {
        command: "+CFUN=?",
        r#type: CommandType::TST,
        fun: parse_cfun_test,
    },
    /* Set frequency band */
    AtCommand {
        command: "+FBAND=",
        r#type: CommandType::SET,
        fun: parse_fband_set,
    },
    AtCommand {
        command: "+FBAND?",
        r#type: CommandType::GET,
        fun: parse_fband_get,
    },
    AtCommand {
        command: "+FBAND=?",
        r#type: CommandType::TST,
        fun: parse_fband_test,
    },
    /* Signal quality */
    AtCommand {
        command: "+CSQ",
        r#type: CommandType::ACT,
        fun: parse_csq_act,
    },
    AtCommand {
        command: "+CSQ=?",
        r#type: CommandType::TST,
        fun: parse_csq_test,
    },
    /* Network registration */
    AtCommand {
        command: "+CREG=",
        r#type: CommandType::SET,
        fun: parse_creg_set,
    },
    AtCommand {
        command: "+CREG?",
        r#type: CommandType::GET,
        fun: parse_creg_get,
    },
    AtCommand {
        command: "+CREG=?",
        r#type: CommandType::TST,
        fun: parse_creg_test,
    },
    /* Data handling */
    AtCommand {
        command: "+SEND=",
        r#type: CommandType::SET,
        fun: parse_send_set,
    },
    AtCommand {
        command: "+SEND?",
        r#type: CommandType::GET,
        fun: parse_send_get,
    },
    AtCommand {
        command: "+SEND=?",
        r#type: CommandType::TST,
        fun: parse_send_test,
    },
];

fn parse_cgsn_act(parameters: &str, mdl: &Mdl) -> Result<ParsingSuccess, ParsingFailure> {
    if parameters.is_empty() {
        let mut res: String<MAX_BUF_SIZ> = String::from("+CGSN: ");
        res.push_str(mdl.serial_number.as_str()).unwrap();
        res.push_str("\r\nOK\r\n").unwrap();
        Ok(ParsingSuccess::Response(res))
    } else {
        Err(ParsingFailure::TrailingChars)
    }
}

fn parse_cgmi_act(parameters: &str, mdl: &Mdl) -> Result<ParsingSuccess, ParsingFailure> {
    if parameters.is_empty() {
        let mut res: String<MAX_BUF_SIZ> = String::from("+CGMI: ");
        res.push_str(mdl.manufacturer.as_str()).unwrap();
        res.push_str("\r\nOK\r\n").unwrap();
        Ok(ParsingSuccess::Response(res))
    } else {
        Err(ParsingFailure::TrailingChars)
    }
}

fn parse_cgmm_act(parameters: &str, mdl: &Mdl) -> Result<ParsingSuccess, ParsingFailure> {
    if parameters.is_empty() {
        let mut res: String<MAX_BUF_SIZ> = String::from("+CGMM: ");
        res.push_str(mdl.model.as_str()).unwrap();
        res.push_str("\r\nOK\r\n").unwrap();
        Ok(ParsingSuccess::Response(res))
    } else {
        Err(ParsingFailure::TrailingChars)
    }
}

fn parse_cgmr_act(parameters: &str, mdl: &Mdl) -> Result<ParsingSuccess, ParsingFailure> {
    if parameters.is_empty() {
        let mut res: String<MAX_BUF_SIZ> = String::from("+CGMR: ");
        res.push_str(mdl.revision.as_str()).unwrap();
        res.push_str("\r\nOK\r\n").unwrap();
        Ok(ParsingSuccess::Response(res))
    } else {
        Err(ParsingFailure::TrailingChars)
    }
}

fn parse_clac_act(parameters: &str, _mdl: &Mdl) -> Result<ParsingSuccess, ParsingFailure> {
    if parameters.is_empty() {
        let mut res: String<MAX_BUF_SIZ> = String::new();

        for command in AT_COMMANDS {
            res.push_str(command.command).unwrap();
            res.push_str("\r\n").unwrap();
        }
        res.push_str("\r\nOK\r\n").unwrap();
        Ok(ParsingSuccess::Response(res))
    } else {
        Err(ParsingFailure::TrailingChars)
    }
}

fn parse_cfun_set(parameters: &str, _mdl: &Mdl) -> Result<ParsingSuccess, ParsingFailure> {
    let params: Vec<&str, 8> = parameters.split(',').collect();

    if params.len() == 1 {
        match params[0].parse::<u8>() {
            Ok(val) => {
                if val == 0 {
                    Ok(ParsingSuccess::Event(MdlEvent::Stop))
                } else if val == 1 {
                    Ok(ParsingSuccess::Event(MdlEvent::Start))
                } else {
                    Err(ParsingFailure::UnexpectedValue)
                }
            }
            Err(_) => Err(ParsingFailure::ParsingError),
        }
    } else {
        Err(ParsingFailure::UnexpectedParameter)
    }
}

fn parse_cfun_get(parameters: &str, mdl: &Mdl) -> Result<ParsingSuccess, ParsingFailure> {
    if parameters.is_empty() {
        let mut res: String<MAX_BUF_SIZ> = String::from("+CFUN: ");
        let status = if mdl.state == MdlState::POWEROFF {
            "0"
        } else {
            "1"
        };
        res.push_str(status).unwrap();
        res.push_str("\r\nOK\r\n").unwrap();

        Ok(ParsingSuccess::Response(res))
    } else {
        Err(ParsingFailure::TrailingChars)
    }
}

fn parse_cfun_test(parameters: &str, _mdl: &Mdl) -> Result<ParsingSuccess, ParsingFailure> {
    if parameters.is_empty() {
        let res = "\r\n+CFUN: (0,1)\r\nOK\r\n";
        Ok(ParsingSuccess::Response(String::from(res)))
    } else {
        Err(ParsingFailure::TrailingChars)
    }
}

fn parse_csq_act(parameters: &str, _mdl: &Mdl) -> Result<ParsingSuccess, ParsingFailure> {
    if parameters.is_empty() {
        Ok(ParsingSuccess::Response(String::from("\r\nOK\r\n")))
    } else {
        Err(ParsingFailure::TrailingChars)
    }
}

fn parse_csq_test(parameters: &str, _mdl: &Mdl) -> Result<ParsingSuccess, ParsingFailure> {
    if parameters.is_empty() {
        Ok(ParsingSuccess::Response(String::from("\r\nOK\r\n")))
    } else {
        Err(ParsingFailure::TrailingChars)
    }
}

fn parse_creg_set(parameters: &str, _mdl: &Mdl) -> Result<ParsingSuccess, ParsingFailure> {
    let params: Vec<&str, 8> = parameters.split(',').collect();

    if params.len() == 1 {
        match params[0].parse::<u8>() {
            Ok(val) => {
                if val == 0 {
                    Ok(ParsingSuccess::Event(MdlEvent::Deregister))
                } else if val == 1 {
                    Ok(ParsingSuccess::Event(MdlEvent::Register))
                } else {
                    Err(ParsingFailure::UnexpectedValue)
                }
            }
            Err(_) => Err(ParsingFailure::ParsingError),
        }
    } else {
        Err(ParsingFailure::UnexpectedParameter)
    }
}

fn parse_creg_get(parameters: &str, mdl: &Mdl) -> Result<ParsingSuccess, ParsingFailure> {
    if parameters.is_empty() {
        let mut res: String<MAX_BUF_SIZ> = String::from("+CREG: ");
        let status = if mdl.state == MdlState::REGISTERED {
            "1"
        } else {
            "0"
        };
        res.push_str(status).unwrap();
        res.push_str("\r\nOK\r\n").unwrap();

        Ok(ParsingSuccess::Response(res))
    } else {
        Err(ParsingFailure::TrailingChars)
    }
}

fn parse_creg_test(parameters: &str, _mdl: &Mdl) -> Result<ParsingSuccess, ParsingFailure> {
    if parameters.is_empty() {
        let res = "\r\n+CREG: (0,1)\r\nOK\r\n";
        Ok(ParsingSuccess::Response(String::from(res)))
    } else {
        Err(ParsingFailure::TrailingChars)
    }
}

fn parse_fband_set(_parameters: &str, _mdl: &Mdl) -> Result<ParsingSuccess, ParsingFailure> {
    todo!();
}

fn parse_fband_get(_parameters: &str, _mdl: &Mdl) -> Result<ParsingSuccess, ParsingFailure> {
    todo!();
}

fn parse_fband_test(_parameters: &str, _mdl: &Mdl) -> Result<ParsingSuccess, ParsingFailure> {
    todo!();
}

fn parse_send_set(_parameters: &str, _mdl: &Mdl) -> Result<ParsingSuccess, ParsingFailure> {
    todo!();
}

fn parse_send_get(_parameters: &str, _mdl: &Mdl) -> Result<ParsingSuccess, ParsingFailure> {
    todo!();
}

fn parse_send_test(_parameters: &str, _mdl: &Mdl) -> Result<ParsingSuccess, ParsingFailure> {
    todo!();
}

fn simulate_at_command(at_command: &str) {
    for ch in at_command.chars() {
        Mdl::send_to_mdl(MdlEvent::Serial(ch));
    }
}

fn send_to_serial(tx: &mut serial::Tx<USART1>, response: &str) {
    rprintln!("{}", response);
    write!(tx, "{}", response).unwrap();
}

pub fn mdl_entry(mdl: &mut Mdl, tx: &mut serial::Tx<USART1>, evt: MdlEvent) {
    if let MdlEvent::Serial(ch) = evt {
        mdl.atci.buf.push(ch).unwrap();

        if ch as u8 == 13 {
            let at_offset = mdl.atci.buf.len() - mdl.atci.buf.trim_start().len();

            let command_line = &mut mdl.atci.buf;

            if command_line[at_offset..].len() > 2
                && command_line[at_offset..2 + at_offset].eq_ignore_ascii_case("at")
            {
                let mut s: String<MAX_BUF_SIZ> = String::from(&command_line[2 + at_offset..]);

                rprintln!("{}", command_line);

                if s.len() > 1 {
                    let s = s.as_mut_str();
                    s.make_ascii_uppercase();

                    let t = s.split(';');

                    for c in t {
                        for command in AT_COMMANDS {
                            let at_command_type = if c.contains("=?") {
                                CommandType::TST
                            } else if c.contains('?') {
                                CommandType::GET
                            } else if c.contains('=') {
                                CommandType::SET
                            } else {
                                CommandType::ACT
                            };

                            if command.r#type == at_command_type
                                && c.trim().starts_with(command.command)
                            {
                                match (command.fun)(&c.trim()[command.command.len()..], mdl) {
                                    Ok(event) => match event {
                                        ParsingSuccess::Response(rsp) => {
                                            send_to_serial(tx, &rsp);
                                        }
                                        ParsingSuccess::Event(evt) => {
                                            Mdl::send_to_mdl(evt);
                                        }
                                    },
                                    Err(err) => {
                                        panic!("Err : {:?}", err);
                                    }
                                }
                            }
                        }
                    }
                } else {
                    send_to_serial(tx, "\r\nOK\r\n");
                }
            } else {
                rprintln!("unexpected :{}", mdl.atci.buf);
                writeln!(tx, "unexpected command {}", mdl.atci.buf).unwrap();
            }

            mdl.atci.buf.clear();
        }
    } else {
        rprintln!("mdl_entry {:?}", evt);
        match &mdl.state {
            MdlState::POWEROFF => match evt {
                MdlEvent::Init => {
                    send_to_serial(tx, "Hello Weightless!\r\n");
                    if !cfg!(mdl) {
                        simulate_at_command("AT+CFUN=1\r");
                    }
                }
                MdlEvent::Start => {
                    Mdl::send_to_rrm(FromMdl::RRM_START_REQ);
                    mdl.state = MdlState::POWERING;
                }
                MdlEvent::Stop => {
                    send_to_serial(tx, "+CME ERROR: already powered off\r\n");
                }
                MdlEvent::Register | MdlEvent::Deregister => {
                    send_to_serial(tx, "+CME ERROR: wrong state\r\n");
                }
                _ => {
                    unreachable!();
                }
            },
            MdlState::POWERING => match evt {
                MdlEvent::Rrm(ToMdl::RRM_START_CNF) => {
                    send_to_serial(tx, "\r\nOK\r\n");
                    mdl.state = MdlState::DEREGISTERED;
                    if !cfg!(mdl) {
                        simulate_at_command("AT+CREG=1\r");
                    }
                }
                MdlEvent::Start | MdlEvent::Stop | MdlEvent::Register | MdlEvent::Deregister => {
                    send_to_serial(tx, "+CME ERROR: wrong state\r\n");
                }
                _ => {
                    unreachable!();
                }
            },
            MdlState::DEPOWERING => match evt {
                MdlEvent::Rrm(ToMdl::RRM_STOP_CNF) => {
                    send_to_serial(tx, "\r\nOK\r\n");
                    mdl.state = MdlState::POWEROFF;
                }
                MdlEvent::Start | MdlEvent::Stop | MdlEvent::Register | MdlEvent::Deregister => {
                    send_to_serial(tx, "+CME ERROR: wrong state\r\n");
                }
                _ => {
                    unreachable!();
                }
            },
            MdlState::DEREGISTERED => match evt {
                MdlEvent::Register => {
                    Mdl::send_to_rrm(FromMdl::RRM_REGISTER_REQ);
                    mdl.state = MdlState::REGISTERING;
                }
                MdlEvent::Stop => {
                    Mdl::send_to_rrm(FromMdl::RRM_STOP_REQ);
                    mdl.state = MdlState::DEPOWERING;
                }
                MdlEvent::Start => {
                    send_to_serial(tx, "+CME ERROR: wrong state\r\n");
                }
                MdlEvent::Deregister => {
                    send_to_serial(tx, "+CME ERROR: already deregistered\r\n");
                }
                _ => {
                    unreachable!();
                }
            },
            MdlState::DEREGISTERING => match evt {
                MdlEvent::Rrm(ToMdl::RRM_DEREGISTER_CNF) => {
                    send_to_serial(tx, "\r\nOK\r\n");
                    mdl.state = MdlState::DEREGISTERED;
                }
                MdlEvent::Start | MdlEvent::Stop | MdlEvent::Register | MdlEvent::Deregister => {
                    send_to_serial(tx, "+CME ERROR: wrong state\r\n");
                }
                _ => {
                    unreachable!();
                }
            }
            MdlState::REGISTERING => match evt {
                MdlEvent::Rrm(ToMdl::RRM_REGISTER_CNF) => {
                    send_to_serial(tx, "\r\nOK\r\n");
                    mdl.state = MdlState::REGISTERED;
                }
                MdlEvent::Start | MdlEvent::Stop | MdlEvent::Register | MdlEvent::Deregister => {
                    send_to_serial(tx, "+CME ERROR: wrong state\r\n");
                }
                _ => {
                    unreachable!();
                }
            }
            MdlState::REGISTERED => match evt {
                MdlEvent::Deregister => {
                    Mdl::send_to_rrm(FromMdl::RRM_DEREGISTER_REQ);
                    mdl.state = MdlState::DEREGISTERING;
                }
                MdlEvent::Stop => {
                    Mdl::send_to_rrm(FromMdl::RRM_STOP_REQ);
                    mdl.state = MdlState::DEPOWERING;
                }
                MdlEvent::Start => {
                    send_to_serial(tx, "+CME ERROR: wrong state\r\n");
                }
                MdlEvent::Register => {
                    send_to_serial(tx, "+CME ERROR: already registered\r\n");
                }
                MdlEvent::Ll(ll::ToMdl::LL_DATA_IND(ref msg)) => {
                    rprintln!("evt {:?}", msg);
                }
                _ => {
                    unreachable!();
                }
            },
        }
    }
}
