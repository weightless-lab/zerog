//#![deny(unsafe_code)]
#![deny(warnings)]
#![allow(clippy::upper_case_acronyms)]
#![allow(clippy::large_enum_variant)]
#![allow(clippy::field_reassign_with_default)]

#![no_main]
#![no_std]

use panic_rtt_target as _;
use rtic::app;

mod bb;
mod l1;
mod ll;
mod mdl;
mod rrm;

#[app(device = stm32l4xx_hal::pac, dispatchers = [EXTI1, EXTI2])]
mod app {

    use dwt_systick_monotonic::DwtSystick;

    use ax5043::AX5x43;
    use stm32l4xx_hal::{
        gpio::PA3,
        gpio::{Alternate, Edge, Floating, Input, Output, PushPull, AF5},
        gpio::{PB12, PB13, PB14, PB15, PC13},
        prelude::*,
        serial::{self, Config, Serial},
        spi::Spi,
        stm32::{SPI2, USART1},
    };

    use stm32l4xx_hal::lptimer::ClockSource::LSE;
    use stm32l4xx_hal::lptimer::Event::AutoReloadMatch;
    use stm32l4xx_hal::lptimer::Event::CompareMatch;
    use stm32l4xx_hal::lptimer::LowPowerTimer;
    use stm32l4xx_hal::lptimer::LowPowerTimerConfig;
    use stm32l4xx_hal::lptimer::PreScaler;
    use stm32l4xx_hal::rcc::ClockSecuritySystem;
    use stm32l4xx_hal::rcc::CrystalBypass;

    use stm32l4xx_hal::timer::{Event, Timer};

    use embedded_hal::spi::MODE_0;

    use rtic::time::duration::{Milliseconds, Seconds};
    use rtt_target::rtt_init_print;

    use crate::l1;
    use crate::l1::L1Event;
    use crate::l1::L1;

    use crate::mdl;
    use crate::mdl::Mdl;
    use crate::mdl::MdlEvent;

    use crate::rrm;
    use crate::rrm::Rrm;
    use crate::rrm::RrmEvent;

    use crate::bb;
    use crate::bb::Bb;
    use crate::bb::BbEvent;

    use crate::ll;
    use crate::ll::Ll;
    use crate::ll::LlEvent;

    /* TODO */
    pub struct DelayHea {}

    pub type MyTimer = LowPowerTimer<stm32l4xx_hal::pac::LPTIM1>;

    pub type AxDriver = 
            AX5x43<
                Spi<
                    SPI2,
                    (
                        PB13<Alternate<AF5, PushPull>>,
                        PB14<Alternate<AF5, PushPull>>,
                        PB15<Alternate<AF5, PushPull>>,
                    ),
                >,
                PB12<Output<PushPull>>,
                DelayHea,
        >;

    impl embedded_hal::blocking::delay::DelayMs<u8> for DelayHea {
        fn delay_ms(&mut self, _d: u8) {
            cortex_m::asm::delay(10000);
        }
    }

    const MONO_HZ: u32 = 80_000_000; // 80 MHz

    #[monotonic(binds = SysTick, default = true)]
    type MyMono = DwtSystick<MONO_HZ>;

    #[resources]
    struct Resources {
        #[task_local]
        ax: AxDriver, 
        #[task_local]
        rf_trx: PC13<Input<Floating>>,
        #[task_local]
        uart_rx: serial::Rx<USART1>,
        #[task_local]
        uart_tx: serial::Tx<USART1>,
        #[task_local]
        l1: L1,
        #[task_local]
        bb: Bb,
        #[task_local]
        ll: Ll,
        #[task_local]
        rrm: Rrm,
        #[task_local]
        mdl: Mdl,
        lptim: LowPowerTimer<stm32l4xx_hal::pac::LPTIM1>,
        tim2: Timer<stm32l4xx_hal::pac::TIM2>,
        #[task_local]
        led: PA3<Output<PushPull>>,
    }

    impl L1 {
        pub fn send_to_rrm(event: l1::ToRrm) {
            rrm_task::spawn(RrmEvent::L1(event)).ok();
        }

        pub fn send_to_bb(event: l1::ToBb) {
            bb_task::spawn(BbEvent::L1(event)).ok();
        }

        pub fn schedule(evt: L1Event) {
            l1_task::spawn(evt).ok();
        }

        pub fn schedule_after(t: u32, evt: L1Event) {
            l1_task::spawn_after(Seconds(t), evt).ok();
        }

        pub fn schedule_after_ms(t: u32, evt: L1Event) {
            l1_task::spawn_after(Milliseconds(t), evt).ok();
        }
    }

    impl Bb {
        pub fn send_to_l1(event: l1::FromBb) {
            l1_task::spawn(L1Event::Bb(event)).ok();
        }

        pub fn send_to_ll(event: bb::ToLl) {
            ll_task::spawn(LlEvent::Bb(event)).ok();
        }

        pub fn send_to_rrm(event: bb::ToRrm) {
            rrm_task::spawn(RrmEvent::Bb(event)).ok();
        }

        pub fn schedule_after(t: u32, evt: BbEvent) {
            bb_task::spawn_after(Seconds(t), evt).ok();
        }
    }

    impl Ll {
        pub fn send_to_bb(event: bb::FromLl) {
            bb_task::spawn(BbEvent::Ll(event)).ok();
        }

        pub fn send_to_rrm(event: ll::ToRrm) {
            rrm_task::spawn(RrmEvent::Ll(event)).ok();
        }

        pub fn send_to_mdl(event: ll::ToMdl) {
            mdl_task::spawn(MdlEvent::Ll(event)).ok();
        }

        pub fn schedule_after(t: u32, evt: LlEvent) -> Result<ll_task::SpawnHandle, LlEvent> {
            ll_task::spawn_after(Seconds(t), evt)
        }
    }

    impl Rrm {
        pub fn send_to_l1(event: l1::FromRrm) {
            l1_task::spawn(L1Event::Rrm(event)).ok();
        }

        pub fn send_to_bb(event: bb::FromRrm) {
            bb_task::spawn(BbEvent::Rrm(event)).ok();
        }

        pub fn send_to_ll(event: ll::FromRrm) {
            ll_task::spawn(LlEvent::Rrm(event)).ok();
        }

        pub fn send_to_rrm(event: RrmEvent) {
            rrm_task::spawn(event).ok();
        }

        pub fn send_to_mdl(event: rrm::ToMdl) {
            mdl_task::spawn(MdlEvent::Rrm(event)).ok();
        }

        pub fn schedule_after(t: u32, evt: RrmEvent) {
            rrm_task::spawn_after(Seconds(t), evt).ok();
        }
    }

    impl Mdl {
        pub fn send_to_rrm(event: rrm::FromMdl) {
            rrm_task::spawn(RrmEvent::Mdl(event)).unwrap();
        }

        pub fn send_to_ll(event: ll::FromMdl) {
            ll_task::spawn(LlEvent::Mdl(event)).unwrap();
        }

        pub fn send_to_mdl(event: MdlEvent) {
            mdl_task::spawn(event).unwrap();
        }

        pub fn schedule_after(t: u32, event: MdlEvent) {
            mdl_task::spawn_after(Seconds(t), event).unwrap();
        }
    }

    #[init]
    fn init(cx: init::Context) -> (init::LateResources, init::Monotonics) {
        // Enable logging
        rtt_init_print!(BlockIfFull);
        //rtt_init_print!();

        let mut dcb = cx.core.DCB;
        let dwt = cx.core.DWT;
        let systick = cx.core.SYST;

        let mono = DwtSystick::new(&mut dcb, dwt, systick, 80_000_000);

        // Device access (Peripheral Access Crate)
        let mut pac = cx.device;

        let mut exti = pac.EXTI;

        let mut rcc = pac.RCC.constrain();
        let mut flash = pac.FLASH.constrain();
        let mut pwr = pac.PWR.constrain(&mut rcc.apb1r1);

        let clocks = rcc
            .cfgr
            .sysclk(80.mhz())
            .lse(CrystalBypass::Disable, ClockSecuritySystem::Disable)
            .freeze(&mut flash.acr, &mut pwr);

        let timer = LowPowerTimer::lptim1(
            pac.LPTIM1,
            LowPowerTimerConfig::default()
                .clock_source(LSE)
                .prescaler(PreScaler::U16),
            &mut rcc.apb1r1,
            &mut rcc.ccipr,
            clocks,
        );

        //let timer = Delay::new(systick, clocks);

        let mut gpioa = pac.GPIOA.split(&mut rcc.ahb2);

        // define RX/TX pins for serial
        let tx_pin = gpioa.pa9.into_af7_pushpull(&mut gpioa.moder, &mut gpioa.otyper, &mut gpioa.afrh);
        let rx_pin = gpioa.pa10.into_af7_pushpull(&mut gpioa.moder, &mut gpioa.otyper, &mut gpioa.afrh);

        let led = gpioa
            .pa3
            .into_push_pull_output(&mut gpioa.moder, &mut gpioa.otyper);

        gpioa.pa5.into_af2_pushpull(&mut gpioa.moder, &mut gpioa.otyper, &mut gpioa.afrl);
        let tim2 = Timer::tim2(pac.TIM2, 100.hz(), clocks, &mut rcc.apb1r1);

        // configure serial
        let mut serial = Serial::usart1(
            pac.USART1,
            (tx_pin, rx_pin),
            Config::default().baudrate(115200.bps()),
            clocks,
            &mut rcc.apb2,
        );

        serial.listen(serial::Event::Rxne);

        let (uart_tx, uart_rx) = serial.split();

        let mut gpioc = pac.GPIOC.split(&mut rcc.ahb2);

        let mut rf_trx = gpioc
            .pc13
            .into_floating_input(&mut gpioc.moder, &mut gpioc.pupdr);

        rf_trx.enable_interrupt(&mut exti);
        rf_trx.make_interrupt_source(&mut pac.SYSCFG, &mut rcc.apb2);
        //rf_trx.trigger_on_edge(&mut exti, Edge::RISING);
        rf_trx.trigger_on_edge(&mut exti, Edge::Rising);
        rf_trx.clear_interrupt_pending_bit();

        let mut gpiob = pac.GPIOB.split(&mut rcc.ahb2);

        let sel = gpiob
            .pb12
            .into_push_pull_output(&mut gpiob.moder, &mut gpiob.otyper);

        let sck2 = gpiob.pb13.into_af5_pushpull(&mut gpiob.moder, &mut gpiob.otyper, &mut gpiob.afrh);
        let miso2 = gpiob.pb14.into_af5_pushpull(&mut gpiob.moder, &mut gpiob.otyper, &mut gpiob.afrh);
        let mosi2 = gpiob.pb15.into_af5_pushpull(&mut gpiob.moder, &mut gpiob.otyper, &mut gpiob.afrh);

        let spi2 = Spi::spi2(
            pac.SPI2,
            (sck2, miso2, mosi2),
            MODE_0,
            5.mhz(),
            clocks,
            &mut rcc.apb1r1,
        );

        let delay_hea = DelayHea {};

        let ax = AX5x43::new(spi2, sel, delay_hea);

        (
            init::LateResources {
                ax,
                uart_rx,
                uart_tx,
                rf_trx,
                l1: L1::new(),
                bb: Bb::new(),
                ll: Ll::new(),
                rrm: Rrm::new(),
                mdl: Mdl::new(),
                lptim: timer,
                tim2,
                led,
            },
            init::Monotonics(mono),
        )
    }

    #[idle]
    fn idle(_cx: idle::Context) -> ! {
        l1_task::spawn(L1Event::Init).unwrap();
        bb_task::spawn(BbEvent::Init).unwrap();
        ll_task::spawn(LlEvent::Init).unwrap();
        rrm_task::spawn(RrmEvent::Init).unwrap();
        mdl_task::spawn(MdlEvent::Init).unwrap();

        loop {
            //cortex_m::asm::wfi();
            cortex_m::asm::nop();
        }
    }

    #[task(binds = EXTI15_10, resources = [rf_trx])]
    fn rf_trx_irq(cx: rf_trx_irq::Context) {
        cx.resources.rf_trx.clear_interrupt_pending_bit();

        l1_task::spawn(L1Event::TRX_IT).ok();
    }

    #[task(resources = [l1, ax, lptim, tim2], capacity = 8)]
    fn l1_task(cx: l1_task::Context, evt: L1Event) {
        l1::l1_entry(cx.resources, evt);
    }

    #[task(resources = [rrm], capacity = 8)]
    fn rrm_task(cx: rrm_task::Context, evt: RrmEvent) {
        rrm::rrm_entry(cx.resources.rrm, evt);
    }

    #[task(resources = [ll], capacity = 8)]
    fn ll_task(cx: ll_task::Context, evt: LlEvent) {
        ll::ll_entry(cx.resources.ll, evt);
    }

    #[task(resources = [bb], capacity = 8)]
    fn bb_task(cx: bb_task::Context, evt: BbEvent) {
        bb::bb_entry(cx.resources.bb, evt);
    }

    // FIXME: capacity value set for testing purpose
    #[task(resources = [mdl, uart_tx], capacity = 16)]
    fn mdl_task(cx: mdl_task::Context, evt: MdlEvent) {
        mdl::mdl_entry(cx.resources.mdl, cx.resources.uart_tx, evt);
    }

    #[task(binds = USART1, priority = 1, resources = [uart_rx])]
    fn uart_task(cx: uart_task::Context) {
        let ch = cx.resources.uart_rx.read();

        if let Ok(ch) = ch {
            mdl_task::spawn(MdlEvent::Serial(ch as char)).ok();
        }
    }

    #[task(binds = LPTIM1, resources= [lptim])]
    fn lptim_task(mut cx: lptim_task::Context) {
        cx.resources.lptim.lock(|tim| {
            if tim.is_event_triggered(CompareMatch) {
                tim.clear_event_flag(CompareMatch);
                l1_task::spawn(L1Event::Timer1).ok();
            }
            if tim.is_event_triggered(AutoReloadMatch) {
                tim.clear_event_flag(AutoReloadMatch);
                l1_task::spawn(L1Event::Timer2).ok();
            }
        });
    }

    #[task(binds = TIM2, resources= [tim2], priority=16)]
    fn tim2_task(mut cx: tim2_task::Context) {
        rtt_target::rprintln!("tim2_task e {}", stm32l4xx_hal::timer::Timer::<stm32l4xx_hal::pac::TIM2>::count());

        cx.resources.tim2.lock(|tim| {
            tim.clear_interrupt(Event::TimeOut);
            l1_task::spawn(L1Event::TX).ok();
        });

    }
}
