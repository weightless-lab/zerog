#![deny(warnings)]
#![allow(non_camel_case_types)]

use heapless::Vec;
use rtt_target::rprintln;

use crate::l1::ToBb::{PHY_DATA_IND, PHY_FRAME_IND};
use crate::l1::{AllocType, DlAlloc};

use crate::l1;
use crate::l1::FromBb;
use crate::l1::PhyReceiveReq;

use crate::ll;
use phy::crc16;

const MAX_BUF_SIZ: usize = 256;
const MAX_LCH_NUM: usize = 16;

const EID_REGISTER: u16 = 0x7FFE;

#[derive(PartialEq, Debug)]
pub enum FromLl {
    BB_STATUS_RSP(StatusRsp),
    BB_DATA_REQ(DataReq),
}

#[derive(PartialEq, Debug)]
pub enum ToLl {
    BB_STATUS_IND(StatusInd),
    BB_TX_IND(TxInd),
    BB_DATA_IND(DataInd),
}

#[derive(PartialEq, Debug)]
pub enum FromRrm {
    BB_CONFIG_REQ(ConfigReq),
}

#[derive(PartialEq, Debug)]
pub enum ToRrm {
    BB_CONFIG_CNF,
}

#[derive(PartialEq, Debug)]
pub struct StatusInd {
    pub sfn: u16,
}

#[derive(PartialEq, Debug)]
pub enum BbEvent {
    Init,
    L1(l1::ToBb),
    Ll(FromLl),
    Rrm(FromRrm),
}

#[derive(PartialEq, Debug)]
pub struct DataReq {
    pub lla: u8,
    pub llb: u8,
    pub buf: Vec<u8, MAX_BUF_SIZ>,
}

#[derive(PartialEq, Debug)]
pub enum DlTrCh {
    BC,
    RC,
    AC(bool),
    AD(bool),
    ID(bool),
    MD,
    UD,
}

#[derive(PartialEq, Debug)]
pub enum SegInfo {
    Start(usize), /* length */
    Cont(usize),  /* offset */
}

#[derive(PartialEq, Debug)]
pub struct DataInd {
    pub trch: DlTrCh,
    pub lla: u8,
    pub llb: u8,
    pub buf: Vec<u8, MAX_BUF_SIZ>,
    pub seg_info: Option<SegInfo>,
}

#[derive(PartialEq, Debug)]
pub struct StatusRsp {
    pub sfn: u16,
    pub lch: Vec<(ll::LogChannelType, usize), MAX_LCH_NUM>,
}

#[derive(PartialEq, Debug)]
pub struct TxInd {
    pub lch: Vec<(ll::LogChannelType, usize), MAX_LCH_NUM>,
}

#[derive(PartialEq, Debug)]
pub struct ConfigReq {
    pub i_eid: Option<u32>,
    pub g_eid: Option<u32>,
    pub even_ra_mcs: u8,
    pub odd_ra_mcs: u8,
}

#[derive(Default, Debug)]
pub struct Bb {
    even_ra_mcs: u8,
    odd_ra_mcs: u8,
    i_eid: Option<u32>,
    g_eid: Option<u32>,
    lch: Vec<(ll::LogChannelType, usize), MAX_LCH_NUM>,
    ruc_sent: bool,
    ul_ra: Vec<(l1::TsDesc, u8), 4>,
}

impl Bb {
    pub fn new() -> Bb {
        Default::default()
    }
}

#[derive(PartialEq, Debug, Clone)]
pub enum EIDType {
    Register,
    Individual,
    All,
    Group,
}

pub fn bb_entry(bb: &mut Bb, event: BbEvent) {
    rprintln!("bb_entry {:x?}", event);

    match event {
        BbEvent::Init => {}
        BbEvent::Rrm(FromRrm::BB_CONFIG_REQ(msg)) => {
            bb.i_eid = msg.i_eid;
            bb.g_eid = msg.g_eid;
            bb.even_ra_mcs = msg.even_ra_mcs;
            bb.odd_ra_mcs = msg.odd_ra_mcs;

            Bb::send_to_rrm(ToRrm::BB_CONFIG_CNF);
        }
        BbEvent::Ll(FromLl::BB_STATUS_RSP(msg)) => {
            let mut bo_ruc = 0;
            let mut bo_others = 0;
            let mut alloc: Vec<DlAlloc, { l1::MAX_ALLOC_NUM }> = Default::default();

            for (lc, bo) in msg.lch.iter() {
                if lc == &ll::LogChannelType::RUC {
                    bo_ruc += bo;
                } else {
                    bo_others += bo;
                }
            }

            if let Some(i_eid) = bb.i_eid {
                let idx = (i_eid as u8) & 0x3;
                let mcs = if (idx % 2) == 0 {
                    bb.even_ra_mcs
                } else {
                    bb.odd_ra_mcs
                };
                alloc
                    .push(DlAlloc {
                        alloc_type: AllocType::DL_RA(idx),
                        mcs: Some(mcs),
                    })
                    .unwrap();
            } else if bb.ruc_sent {
                /* FIXME */
                for idx in 0..1 {
                    let mcs = if (idx % 2) == 0 {
                        bb.even_ra_mcs
                    } else {
                        bb.odd_ra_mcs
                    };
                    alloc
                        .push(DlAlloc {
                            alloc_type: AllocType::DL_RA(idx),
                            mcs: Some(mcs),
                        })
                        .unwrap();
                }
            }

            if let Some(i_eid) = bb.i_eid {
                if bo_others > 0 {
                    let idx = (i_eid as u8) & 0x7;
                    let mcs = if (idx % 2) == 0 {
                        bb.even_ra_mcs
                    } else {
                        bb.odd_ra_mcs
                    };
                    alloc
                        .push(DlAlloc {
                            alloc_type: AllocType::UL_RA(idx),
                            mcs: Some(mcs),
                        })
                        .unwrap();
                }
            } else if bo_ruc > 1 {
                /* FIXME */
                for idx in 0..1 {
                    let mcs = if (idx % 2) == 0 {
                        bb.even_ra_mcs
                    } else {
                        bb.odd_ra_mcs
                    };
                    alloc
                        .push(DlAlloc {
                            alloc_type: AllocType::UL_RA(idx),
                            mcs: Some(mcs),
                        })
                        .unwrap();
                }
            }
            bb.lch = msg.lch;

            Bb::send_to_l1(FromBb::PHY_RECEIVE_REQ(PhyReceiveReq {
                sfn: msg.sfn,
                alloc,
            }));
        }

        BbEvent::Ll(FromLl::BB_DATA_REQ(msg)) => {
            /* TODO SFN */
            if let Ok(bpd) = encode_bpd(EIDType::Register, &msg.buf) {
                let mut msg: l1::PhyTransmitReq = Default::default();
                let alloc = &bb.ul_ra[0];
                msg.alloc
                    .push(l1::UlAlloc {
                        data: bpd,
                        mcs: alloc.1,
                        alloc_type: AllocType::UL_ALLOC(alloc.0),
                    })
                    .unwrap();
                Bb::send_to_l1(FromBb::PHY_TRANSMIT_REQ(msg));

                bb.ruc_sent = true;
            }
        }

        BbEvent::L1(PHY_FRAME_IND(msg)) => {
            Bb::send_to_ll(ToLl::BB_STATUS_IND(StatusInd { sfn: msg.sfn }));
        }

        BbEvent::L1(PHY_DATA_IND(msg)) => match msg.data.alloc_type {
            AllocType::DL_RA(_) => {
                let mut alloc: Vec<DlAlloc, { l1::MAX_ALLOC_NUM }> = Default::default();

                for res in decode_dl_ra(bb, msg.sfn, &msg.data.data_buf) {
                    alloc
                        .push(DlAlloc {
                            alloc_type: AllocType::DL_ALLOC((res.0, EIDType::Register)), /* FIXME should come from eid */
                            mcs: Some(res.1),
                        })
                        .unwrap();
                }

                Bb::send_to_l1(FromBb::PHY_RECEIVE_REQ(PhyReceiveReq {
                    sfn: msg.sfn,
                    alloc,
                }));
            }
            AllocType::UL_RA(_) => {
                if let Ok(ul_ra) = decode_ul_ra(msg.sfn, &msg.data.data_buf) {
                    bb.ul_ra = ul_ra;

                    if !bb.ul_ra.is_empty() {
                        /* FIXME volume per UL_RA */
                        Bb::send_to_ll(ToLl::BB_TX_IND(TxInd {
                            lch: bb.lch.clone(),
                        }));
                    }
                }
            }
            AllocType::DL_ALLOC((_, eid_type)) => {
                if let Ok(data_ind) = decode_bpd(eid_type, &msg.data.data_buf) {
                    Bb::send_to_ll(ToLl::BB_DATA_IND(data_ind));
                }
            }
            _ => {
                todo!();
            }
        },
    }
}

fn encode_bpd(eid_type: EIDType, data: &[u8]) -> Result<Vec<u8, MAX_BUF_SIZ>, ()> {
    let mut bpd = Vec::<u8, MAX_BUF_SIZ>::new();

    match eid_type {
        EIDType::Register => {
            bpd.resize(2, 0).unwrap();
            bpd[0] = data.len() as u8;
            bpd[1] = 1; /* CA Type = 0, CA flags = 0, last BPD in allocation */
            bpd.extend_from_slice(data).unwrap();

            let crc;

            unsafe {
                crc = crc16(bpd.as_ptr(), data.len() as u16 + 2);
            }

            bpd.push((crc >> 8) as u8).unwrap();
            bpd.push((crc & 0xff) as u8).unwrap();

            Ok(bpd)
        }
        EIDType::Individual | EIDType::All | EIDType::Group => {
            todo!();
        }
    }
}

fn decode_bpd(eid_type: EIDType, data: &[u8]) -> Result<DataInd, ()> {
    if !data.is_empty() && usize::from(data[0]) < data.len() {
        let len = data[0] as usize;
        let flags = data[1];
        let lla = data[2];
        let llb = data[3];

        let mut trch = match eid_type {
            EIDType::Register => DlTrCh::RC,
            EIDType::Individual => {
                match flags & 0x3 {
                    0b00 => DlTrCh::AD(true),
                    0b01 => DlTrCh::UD,
                    0b10 => DlTrCh::AC(true),
                    0b11 => DlTrCh::MD, /* called tunneled in table 3.3 section 3.5.1 */
                    _ => {
                        unreachable!();
                    }
                }
            }
            EIDType::All => DlTrCh::BC,
            EIDType::Group => DlTrCh::MD,
        };

        match (flags >> 2) & 0x7 {
            0b100 => {
                rprintln!("Complete Message without Segments");
                /* Length (8bits) / Flags (8bits) / LLa (8bits) / LLb (8bits) / Header CRC (8bits) / Complete msg / CRC (16 bits) */

                let header_crc = data[4];

                let crc = header_crc; /* TODO */

                if crc == header_crc {
                    let _crc;
                    unsafe {
                        _crc = crc16(data[5..].as_ptr(), len as u16);
                    }
                    /* TODO compare with the 2 last bytes */

                    Ok(DataInd {
                        lla,
                        llb,
                        trch,
                        seg_info: None,
                        buf: Vec::from_slice(&data[5..=len + 5])?,
                    })
                } else {
                    rprintln!("header crc check failed");
                    Err(())
                }
            }
            0b010 => {
                rprintln!("First Partial Message without Segments");

                let _header_crc = data[6];

                let message_len = ((data[4] as usize) << 8) + data[5] as usize;

                Ok(DataInd {
                    lla,
                    llb,
                    trch,
                    seg_info: Some(SegInfo::Start(message_len)),
                    buf: Vec::from_slice(&data[5..=len + 5])?, /* FIXME */
                })
            }

            0b000 => {
                rprintln!("Continued Partial Message without Segments");

                let _header_crc = data[6];

                let offset = ((data[4] as usize) << 8) + data[5] as usize;

                Ok(DataInd {
                    lla,
                    llb,
                    trch,
                    seg_info: Some(SegInfo::Cont(offset)),
                    buf: Vec::from_slice(&data[5..=len + 5])?, /* FIXME */
                })
            }
            0b110 => {
                match trch {
                    DlTrCh::AC(ref mut is_data) | DlTrCh::AD(ref mut is_data) => {
                        *is_data = false;
                    }
                    DlTrCh::MD => {
                        //trch = DlTrCh::ID(false);
                    }
                    _ => {
                        panic!();
                    }
                }
                todo!("Complete Message without Segments");
            }
            0b111 => {
                match trch {
                    DlTrCh::AC(ref mut is_data) | DlTrCh::AD(ref mut is_data) => {
                        *is_data = false;
                    }
                    DlTrCh::MD => {
                        //trch = DlTrCh::ID(false);
                    }
                    _ => {
                        panic!();
                    }
                }
                todo!("Complete Message with Segments");
            }
            0b001 => {
                todo!("Continued Partial Message with Segments");
            }
            0b101 => {
                todo!("Complete Message with Segments");
            }
            0b011 => {
                todo!("First Partial Message with Segments");
            }
            _ => {
                panic!("unexpected bpd format");
            }
        }
    } else {
        Err(())
    }
}

fn decode_dl_ra(_bb: &Bb, sfn: u16, data: &[u8]) -> Vec<(l1::TsDesc, u8), 4> {
    let mut res = Vec::<(l1::TsDesc, u8), 4>::new();

    let pdu = &data[1..];

    if u16::from(pdu[0]) == sfn & 0xff {
        let dl_ra_flags = pdu[1];
        let dl_start_ts_h = pdu[2] as u16;
        let dl_ra_num = pdu[3] as usize;

        let dl_start_ts_l = (dl_ra_flags >> 6) as u16;

        let start_ts = (dl_start_ts_h << 2) + dl_start_ts_l;
        let _sib_flag = dl_ra_flags & 0x01;

        for i in 0..dl_ra_num {
            let dl_ra_desc = pdu[4 + 5 * i];

            let dl_ra_dst_eid0_h = ((pdu[5 + 5 * i] as u16) << 8) + pdu[6 + 5 * i] as u16;
            let dl_ra_dst_eid1_h = ((pdu[7 + 5 * i] as u16) << 8) + pdu[8 + 5 * i] as u16;

            /* TODO check i_eid, g_eid, s_eid (eid_register), */
            if dl_ra_dst_eid0_h == 0xffff {
                let nb_ts = dl_ra_desc & 0xf;
                let mcs = pdu[9 + 5 * dl_ra_num + i] & 0xf;
                res.push((
                    l1::TsDesc {
                        start_ts,
                        nb_ts,
                        channel_idx: None,
                    },
                    mcs,
                ))
                .unwrap();
            }

            if dl_ra_dst_eid1_h == 0xffff {
                let nb_ts = dl_ra_desc >> 0x4;
                let mcs = pdu[9 + 5 * dl_ra_num + i] >> 4;
                res.push((
                    l1::TsDesc {
                        start_ts,
                        nb_ts,
                        channel_idx: None,
                    },
                    mcs,
                ))
                .unwrap();
            }
        }
    } else {
        panic!("inconsistent sfn");
    }

    res
}

fn decode_ul_ra(sfn: u16, data: &[u8]) -> Result<Vec<(l1::TsDesc, u8), 4>, ()> {
    if data.len() > (3 + 1) {
        let pdu = &data[1..];

        if u16::from(pdu[0]) == sfn & 0xff {
            let ul_ra_flags = pdu[1];
            let ul_start_ts_h = pdu[2] as u16;
            let ul_ra_num = pdu[3] as usize;
            let ul_start_ts_l = (ul_ra_flags >> 6) as u16;
            let start_ts = (ul_start_ts_h << 2) + ul_start_ts_l;

            let mut res = Vec::<(l1::TsDesc, u8), 4>::new();

            if pdu.len() >= 4 + 3 * ul_ra_num {
                for i in 0..ul_ra_num {
                    let ul_ra_desc = pdu[4 + 3 * i];

                    let _sib_flag = ul_ra_flags & 0x01;

                    let ul_ra_dest_eid_h = ((pdu[5 + 3 * i] as u16) << 8) + pdu[6 + 3 * i] as u16;

                    if ul_ra_dest_eid_h == EID_REGISTER {
                        let nb_ts = ul_ra_desc >> 0x4;
                        let mcs = ul_ra_desc & 0xf;

                        if res
                            .push((
                                l1::TsDesc {
                                    start_ts,
                                    nb_ts,
                                    channel_idx: None,
                                },
                                mcs,
                            ))
                            .is_err()
                        {
                            return Err(());
                        }
                    }
                }
            }
            Ok(res)
        } else {
            panic!("inconsistent sfn");
        }
    } else {
        Err(())
    }
}
