#![allow(non_camel_case_types)]

use heapless::{Deque, Vec};
use rtt_target::rprintln;

use crate::{bb, l1, ll, rrm};
use enum_dispatch::enum_dispatch;

const MAX_BUF_SIZ: usize = 256;
const MAX_RSSI_SCAN_RESULTS: usize = 32;
const MAX_PROC_QUEUE_SIZ: usize = 4;

const UUEID_SIZ: usize = 16;

//const REGISTRATION_REQUEST_WITH_NAI: u8 = 0x01;
const REGISTRATION_REQUEST_WITH_IEID: u8 = 0x02;
const REGISTRATION_REQUEST_WITH_UUEID: u8 = 0x03;
const REGISTRATION_CONFIRM: u8 = 0x06;
//const REGISTRATION_REJECT_WITH_NAI: u8 = 0x07;
//const REGISTRATION_REJECT_WITH_IEID: u8 = 0x08;
//const REGISTRATION_REJECT_WITH_UUEID: u8 = 0x09;

#[derive(PartialEq, Debug)]
pub enum FromMdl {
    RRM_START_REQ,
    RRM_STOP_REQ,
    RRM_REGISTER_REQ,
    RRM_DEREGISTER_REQ,
}

#[derive(PartialEq, Debug)]
pub enum ToMdl {
    RRM_START_CNF,
    RRM_STOP_CNF,
    RRM_REGISTER_CNF,
    RRM_DEREGISTER_CNF,
}

#[derive(PartialEq, Debug)]
enum RegState {
    //Registered,
    NotRegistered,
}

#[derive(PartialEq, Debug)]
enum State {
    Null,
    PoweredOff,
    NoCell(RegState),
    CellCamped(RegState),
}

impl Default for State {
    fn default() -> State {
        State::Null
    }
}

#[derive(Default, Debug)]
struct Config {
    state: State,
    rssi_scan: Vec<l1::Result, MAX_RSSI_SCAN_RESULTS>,
    warfcn: u16,
    cell_info: l1::CellInfo,
    /* TODO define a proper structure holding ED configuration */
    ll_configured: bool,

    uu_eid: Vec<u8, UUEID_SIZ>,
    i_eid: Option<u32>,
    g_eid: Option<u32>,
}

#[derive(Default)]
pub struct Rrm {
    config: Config,
    proc: ProcScheduler,
}

impl Rrm {
    pub fn new() -> Rrm {
        Default::default()
    }
}

#[derive(PartialEq, Debug)]
pub enum RrmEvent {
    Init,
    L1(l1::ToRrm),
    Ll(ll::ToRrm),
    Bb(bb::ToRrm),
    Mdl(rrm::FromMdl),
    GuardTimer,
    ProcEnded,
}

#[derive(PartialEq, Debug)]
enum ProcStartSuccess {
    STARTED,
    DEFERRED_AFTER(Proc),
}

#[enum_dispatch(Proc)]
trait Procedure {
    fn start(&mut self, config: &mut Config) -> Result<ProcStartSuccess, ()>;
    fn receive(&mut self, evt: rrm::RrmEvent, config: &mut Config);
    fn end(&mut self);
    fn stop(&mut self);
}

#[enum_dispatch]
#[derive(PartialEq, Debug)]
enum Proc {
    start(StartProc),
    config(ConfigProc),
    rssi_scan(RssiScanProc),
    cell_sel(CellSelProc),
    registration(RegProc),
    /* security proc */
    /* measurement */
    /* cell reselection */
    /* deregistration */
    /* stop(Stop), */
}

#[derive(Default)]
struct ProcScheduler {
    ongoing: Option<Proc>,
    queue: Deque<Proc, MAX_PROC_QUEUE_SIZ>,
}

/* TODO review states, lock mechanism and concurrency */

impl ProcScheduler {
    fn start(&mut self, mut p: Proc, config: &mut Config) -> Result<(), ()> {
        if let Some(ref mut _pp) = self.ongoing {
            /* TODO handle concurrency between ongoing proc and new proc (preempt, reject, enqueue */
            self.queue.push_back(p).unwrap();
        } else {
            let res = p.start(config)?;

            if let ProcStartSuccess::DEFERRED_AFTER(new_proc) = res {
                rprintln!("PROC: {:?} deferred after {:?}", p, new_proc);
                self.queue.push_front(p).unwrap();

                self.start(new_proc, config)?;
            } else {
                self.ongoing = Some(p);
            }
        }

        Ok(())
    }

    fn receive(&mut self, evt: rrm::RrmEvent, config: &mut Config) {
        if let Some(ref mut p) = self.ongoing {
            p.receive(evt, config);
        }
    }

    fn stop(&mut self, evt: rrm::RrmEvent) {
        todo!("STOP {:?}", evt);
    }

    fn end(&mut self, _evt: rrm::RrmEvent, config: &mut Config) {
        /* TODO move code below in the caller */
        if let Some(ref p) = self.ongoing {
            rprintln!("PROC: {:?} Completed succesfully", p);
        }
        self.ongoing = None;
        if let Some(proc) = self.queue.pop_front() {
            self.start(proc, config).unwrap();
        }
    }
}

#[derive(Default, PartialEq, Debug)]
struct StartProc {}

impl Procedure for StartProc {
    fn start(&mut self, config: &mut Config) -> Result<ProcStartSuccess, ()> {
        match config.state {
            State::PoweredOff => {
                Rrm::send_to_l1(l1::FromRrm::CPHY_START_REQ(l1::CphyStartReq {}));
                Ok(ProcStartSuccess::STARTED)
            }
            _ => {
                unreachable!();
            }
        }
    }

    fn receive(&mut self, evt: rrm::RrmEvent, config: &mut Config) {
        match config.state {
            State::PoweredOff => match evt {
                RrmEvent::L1(l1::ToRrm::CPHY_START_CNF(_cnf)) => {
                    config.state = State::NoCell(RegState::NotRegistered);
                    Rrm::send_to_mdl(ToMdl::RRM_START_CNF);
                    self.end();
                }
                _ => {
                    unreachable!();
                }
            },
            _ => {
                unreachable!();
            }
        }
    }

    fn end(&mut self) {
        Rrm::send_to_rrm(RrmEvent::ProcEnded);
    }

    fn stop(&mut self) {
        todo!();
    }
}

#[derive(Default, PartialEq, Debug)]
struct RssiScanProc {}

impl Procedure for RssiScanProc {
    fn start(&mut self, config: &mut Config) -> Result<ProcStartSuccess, ()> {
        match config.state {
            State::NoCell(_) => {
                let mut msg: l1::CphyRssiScanReq = Default::default();

                /* TODO get candidate warfcn from MDL */
                msg.nb_freq = 1;
                msg.warfcn_start = 8700;

                Rrm::send_to_l1(l1::FromRrm::CPHY_RSSI_SCAN_REQ(msg));
                Ok(ProcStartSuccess::STARTED)
            }
            _ => {
                unreachable!();
            }
        }
    }

    fn receive(&mut self, evt: rrm::RrmEvent, config: &mut Config) {
        match config.state {
            State::NoCell(_) => match evt {
                RrmEvent::L1(l1::ToRrm::CPHY_RSSI_SCAN_CNF(cnf)) => {
                    config
                        .rssi_scan
                        .push(l1::Result {
                            warfcn: cnf.result[0].warfcn,
                            rssi: cnf.result[0].rssi,
                        })
                        .unwrap();
                    self.end();
                }
                _ => {
                    unreachable!();
                }
            },
            _ => {
                unreachable!();
            }
        }
    }

    fn end(&mut self) {
        Rrm::send_to_rrm(RrmEvent::ProcEnded);
    }

    fn stop(&mut self) {
        todo!("RssiScan stop");
    }
}

#[derive(PartialEq, Debug)]
enum ConfigState {
    Null,
    WaitForBbConfigCnf,
    WaitForLlConfigCnf,
    WaitForL1ConfigCnf,
}

impl Default for ConfigState {
    fn default() -> ConfigState {
        ConfigState::Null
    }
}

#[derive(Default, PartialEq, Debug)]
struct ConfigProc {
    state: ConfigState,
}

impl Procedure for ConfigProc {
    fn start(&mut self, config: &mut Config) -> Result<ProcStartSuccess, ()> {
        Rrm::send_to_bb(bb::FromRrm::BB_CONFIG_REQ(bb::ConfigReq {
            i_eid : config.i_eid,
            g_eid : config.g_eid,
            even_ra_mcs: config.cell_info.even_ra_mcs,
            odd_ra_mcs: config.cell_info.odd_ra_mcs,
        }));

        self.state = ConfigState::WaitForBbConfigCnf;

        Ok(ProcStartSuccess::STARTED)
    }

    fn receive(&mut self, event: rrm::RrmEvent, config: &mut Config) {
        match self.state {
            ConfigState::WaitForBbConfigCnf => match event {
                RrmEvent::Bb(_) => {
                    Rrm::send_to_ll(ll::FromRrm::LL_CONFIG_REQ(ll::ConfigReq {
                        log_channel_type: ll::LogChannelType::BUC,
                    }));
                    Rrm::send_to_ll(ll::FromRrm::LL_CONFIG_REQ(ll::ConfigReq {
                        log_channel_type: ll::LogChannelType::RUC,
                    }));
                    Rrm::send_to_ll(ll::FromRrm::LL_CONFIG_REQ(ll::ConfigReq {
                        log_channel_type: ll::LogChannelType::UAC,
                    }));
                    self.state = ConfigState::WaitForLlConfigCnf;
                }
                _ => {
                    unreachable!();
                }
            },
            ConfigState::WaitForLlConfigCnf => match event {
                RrmEvent::Ll(x) => {
                    if let ll::ToRrm::LL_CONFIG_CNF(cnf) = x {
                        if cnf.log_channel_type == ll::LogChannelType::UAC {
                            Rrm::send_to_l1(l1::FromRrm::CPHY_CONFIG_REQ(l1::CphyConfigReq {
                                warfcn: config.warfcn,
                                frame_duration: config.cell_info.frame_duration,
                                ..Default::default()
                            }));
                            self.state = ConfigState::WaitForL1ConfigCnf;
                        }
                    }
                }
                _ => {
                    unreachable!();
                }
            },
            ConfigState::WaitForL1ConfigCnf => match event {
                RrmEvent::L1(_) => {
                    config.ll_configured = true;
                    self.end();
                }
                _ => {
                    unreachable!();
                }
            },
            _ => {
                unreachable!();
            }
        }
    }

    fn end(&mut self) {
        Rrm::send_to_rrm(RrmEvent::ProcEnded);
    }

    fn stop(&mut self) {
        todo!();
    }
}

impl Procedure for CellSelProc {
    fn start(&mut self, config: &mut Config) -> Result<ProcStartSuccess, ()> {
        if config.rssi_scan.is_empty() {
            Ok(ProcStartSuccess::DEFERRED_AFTER(Proc::rssi_scan(
                Default::default(),
            )))
        } else {
            self.candidate_warfcn = config.rssi_scan[0].warfcn;
            //Rrm::schedule_after(CellSelProc::TIMEOUT, RrmEvent::GuardTimer(0));
            self.state = CellSelState::WaitForCellFindCnf;
            Rrm::send_to_l1(l1::FromRrm::CPHY_CELL_SEARCH_REQ(l1::CphyCellSearchReq {
                warfcn: self.candidate_warfcn,
            }));
            Ok(ProcStartSuccess::STARTED)
        }
    }

    fn receive(&mut self, evt: rrm::RrmEvent, config: &mut Config) {
        match evt {
            RrmEvent::L1(l1::ToRrm::CPHY_CELL_SEARCH_CNF(cnf)) => {
                /* TODO check bsn and RSSI and check if the cell is suitable */

                config.warfcn = self.candidate_warfcn;
                config.state = State::CellCamped(RegState::NotRegistered);

                config.cell_info = cnf.cell_info;
                self.end();
            }
            _ => {
                panic!();
            }
        }
    }

    fn end(&mut self) {
        Rrm::send_to_rrm(RrmEvent::ProcEnded);
    }

    fn stop(&mut self) {
        todo!("CellSelProc stop");
    }
}

#[derive(Default, PartialEq, Debug)]
struct RegProc {}

impl Procedure for RegProc {
    fn start(&mut self, config: &mut Config) -> Result<ProcStartSuccess, ()> {
        match config.state {
            State::NoCell(RegState::NotRegistered) => Ok(ProcStartSuccess::DEFERRED_AFTER(
                Proc::cell_sel(Default::default()),
            )),
            State::CellCamped(RegState::NotRegistered) => {
                if !config.ll_configured {
                    Ok(ProcStartSuccess::DEFERRED_AFTER(Proc::config(
                        Default::default(),
                    )))
                } else {
                    rprintln!("SEND LL DATA REQ {:?}", config.state);

                    let mut registration_request = Vec::<u8, MAX_BUF_SIZ>::new();

                    if let Some(_ied) = &config.i_eid {
                        registration_request.push(REGISTRATION_REQUEST_WITH_IEID).unwrap();
                    } else {
                        registration_request.push(REGISTRATION_REQUEST_WITH_UUEID).unwrap();
                        registration_request
                            .extend_from_slice(&config.uu_eid)
                            .unwrap();
                    }

                    Rrm::send_to_ll(ll::FromRrm::LL_DATA_REQ(ll::DataReq {
                        log_channel_type: ll::LogChannelType::RUC,
                        buf: registration_request,
                    }));

                    Ok(ProcStartSuccess::STARTED)
                }
            }
            _ => {
                todo!("state {:?}", config.state);
            }
        }
    }

    fn receive(&mut self, evt: rrm::RrmEvent, config: &mut Config) {
        rprintln!("RegProc receive {:?}", evt);

        /* TODO decode PDU */

        match &evt {
            RrmEvent::Ll(ll::ToRrm::LL_DATA_IND(msg)) => {
                if msg.buf[0] == REGISTRATION_CONFIRM {
                    if config.uu_eid[0..16] == msg.buf[4..20] {
                        let i_eid = ((msg.buf[1] as u32) << 16)
                            + ((msg.buf[2] as u32) << 8)
                            + (msg.buf[3] as u32);
                        config.i_eid = Some(i_eid);

                        Rrm::send_to_bb(bb::FromRrm::BB_CONFIG_REQ(bb::ConfigReq {
                            i_eid: config.i_eid,
                            g_eid: config.g_eid,
                            even_ra_mcs: config.cell_info.even_ra_mcs,
                            odd_ra_mcs: config.cell_info.odd_ra_mcs,
                        }));

                        /* handle failure case */

                        Rrm::send_to_mdl(ToMdl::RRM_REGISTER_CNF);
                        self.end();
                    }
                } else {
                    todo!();
                }
            }
            _ => {
                panic!();
            }
        }
    }

    fn stop(&mut self) {
        todo!("RegProc stop");
    }
    fn end(&mut self) {
        Rrm::send_to_rrm(RrmEvent::ProcEnded);
    }
}

#[derive(Default, PartialEq, Debug)]
struct CellSelProc {
    state: CellSelState,
    candidate_warfcn: u16,
}

#[derive(PartialEq, Debug)]
enum CellSelState {
    Null,
    WaitForCellFindCnf,
}

impl Default for CellSelState {
    fn default() -> CellSelState {
        CellSelState::Null
    }
}

pub fn rrm_entry(rrm: &mut Rrm, event: RrmEvent) {
    rprintln!("rrm_entry {:x?}", event);

    match rrm.config.state {
        State::Null => match &event {
            RrmEvent::Init => {
                rrm.config.state = State::PoweredOff;
                rrm.config.uu_eid =
                    Vec::from_slice(&[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15])
                        .unwrap();
            }
            _ => {
                unreachable!();
            }
        },
        _ => match &event {
            RrmEvent::Mdl(FromMdl::RRM_START_REQ) => {
                rrm.proc
                    .start(Proc::start(Default::default()), &mut rrm.config)
                    .unwrap();
            }
            RrmEvent::Mdl(FromMdl::RRM_REGISTER_REQ) => {
                rrm.proc
                    .start(Proc::registration(Default::default()), &mut rrm.config)
                    .unwrap();
            }
            RrmEvent::L1(_) | RrmEvent::Bb(_) | RrmEvent::Ll(_) => {
                rrm.proc.receive(event, &mut rrm.config);
            }
            RrmEvent::GuardTimer => {
                rrm.proc.stop(event);
            }
            RrmEvent::ProcEnded => {
                rrm.proc.end(event, &mut rrm.config);
            }
            _ => {
                todo!();
            }
        },
    }
}
