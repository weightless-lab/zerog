#![deny(warnings)]
#![allow(non_camel_case_types)]

use heapless::{FnvIndexMap, Vec};

use rtt_target::rprintln;

use crate::bb;

const MAX_BUF_SIZ: usize = 256;
const MAX_LCH_NUM: usize = 16;

#[derive(PartialEq, Debug)]
pub enum FromRrm {
    LL_CONFIG_REQ(ConfigReq),
    LL_RELEASE_REQ(LogChannelType),

    LL_DATA_REQ(DataReq),
}

#[derive(PartialEq, Debug)]
pub enum ToRrm {
    LL_CONFIG_CNF(ConfigCnf),
    LL_RELEASE_CNF(ReleaseCnf),

    LL_DATA_IND(DataInd),
    LL_DATA_CNF(DataCnf),
}

#[derive(PartialEq, Debug)]
pub enum FromMdl {
    LL_DATA_REQ(DataReq),
}

#[derive(PartialEq, Debug)]
pub enum ToMdl {
    LL_DATA_IND(DataInd),
    LL_DATA_CNF(DataCnf),
}

#[derive(PartialEq, Eq, Debug, Copy, Clone)]
pub enum LogChannelType {
    BUC,
    RUC,
    UAC,
    IAD(u8),
    IUD(u8),
    MAD(u8),
    MUD(u8),
    UAD,
    UUD,
}

impl hash32::Hash for LogChannelType {
    fn hash<H>(&self, state: &mut H)
    where
        H: hash32::Hasher,
    {
        let tmp = match *self {
            LogChannelType::BUC => [0, 0],
            LogChannelType::RUC => [1, 0],
            LogChannelType::UAC => [2, 0],
            LogChannelType::IAD(instance) => [3, instance],
            LogChannelType::IUD(instance) => [4, instance],
            LogChannelType::MAD(instance) => [5, instance],
            LogChannelType::MUD(instance) => [6, instance],
            LogChannelType::UAD => [7, 0],
            LogChannelType::UUD => [8, 0],
        };

        state.write(&tmp);
    }
}

#[derive(PartialEq, Debug)]
pub struct DataReq {
    pub log_channel_type: LogChannelType,
    pub buf: Vec<u8, MAX_BUF_SIZ>,
}

#[derive(PartialEq, Debug)]
pub struct DataInd {
    pub log_channel_type: LogChannelType,
    pub buf: Vec<u8, MAX_BUF_SIZ>,
}

#[derive(PartialEq, Debug)]
pub enum DataCnf {
    Success,
    Failure,
}

#[derive(PartialEq, Debug)]
pub struct ConfigReq {
    pub log_channel_type: LogChannelType,
}

#[derive(PartialEq, Debug)]
pub struct ConfigCnf {
    pub log_channel_type: LogChannelType,
}

#[derive(PartialEq, Debug)]
pub struct ReleaseCnf {}

#[derive(PartialEq, Debug)]
pub enum LlEvent {
    Init,
    Rrm(FromRrm),
    Mdl(FromMdl),
    Bb(bb::ToLl),
}

#[derive(Debug, Default)]
struct LlEntity {
    queue: Vec<Vec<u8, MAX_BUF_SIZ>, 4>,
    bo: usize,
}

#[derive(Default)]
pub struct Ll {
    lch: FnvIndexMap<LogChannelType, LlEntity, MAX_LCH_NUM>,
}

impl Ll {
    pub fn new() -> Ll {
        Default::default()
    }
}

pub fn ll_entry(ll: &mut Ll, event: LlEvent) {
    rprintln!("ll_entry {:?}", event);

    match event {
        LlEvent::Init => {}
        LlEvent::Rrm(FromRrm::LL_DATA_REQ(msg)) | LlEvent::Mdl(FromMdl::LL_DATA_REQ(msg)) => {
            if let Some(entity) = ll.lch.get_mut(&msg.log_channel_type) {
                entity.bo += msg.buf.len();
                if let Err(_err) = entity.queue.push(msg.buf) {}
            } else {
            }
        }
        LlEvent::Bb(bb::ToLl::BB_DATA_IND(msg)) => {
            /* TODO */
            let lch = match msg.trch {
                bb::DlTrCh::RC => {
                    LogChannelType::RUC
                }
                _ => {
                    panic!();
                }
            };

            if lch != LogChannelType::RUC {
                Ll::send_to_mdl(ToMdl::LL_DATA_IND(DataInd {
                    log_channel_type: lch,
                    buf : msg.buf,
                }));
            } else {
                Ll::send_to_rrm(ToRrm::LL_DATA_IND(DataInd {
                    log_channel_type: lch,
                    buf : msg.buf,
                }));
            }
        }
        LlEvent::Bb(bb::ToLl::BB_STATUS_IND(msg)) => {
            let mut lch: Vec<(LogChannelType, usize), 16> = Default::default();

            for (k, v) in ll.lch.iter() {
                if v.bo > 0 {
                    lch.push((*k, v.bo)).ok();
                }
            }

            Ll::send_to_bb(bb::FromLl::BB_STATUS_RSP(bb::StatusRsp {
                lch,
                sfn: msg.sfn,
            }));
        }
        LlEvent::Bb(bb::ToLl::BB_TX_IND(msg)) => {
            for (k, _v) in msg.lch.iter() {
                if let Some(entity) = ll.lch.get_mut(k) {
                    rprintln!("msg {:?} {:?}", k, entity);

                    /* FIXME */

                    if !entity.queue.is_empty() {
                        entity.bo = 0;
                        Ll::send_to_bb(bb::FromLl::BB_DATA_REQ(bb::DataReq {
                            lla: 0,
                            llb: 0,
                            buf: entity.queue.pop().unwrap(),
                        }));
                    }
                }
            }
        }
        LlEvent::Rrm(FromRrm::LL_CONFIG_REQ(msg)) => {
            ll.lch.insert(msg.log_channel_type, Default::default()).ok();
            Ll::send_to_rrm(ToRrm::LL_CONFIG_CNF(ConfigCnf {
                log_channel_type: msg.log_channel_type,
            }));
        }
        LlEvent::Rrm(FromRrm::LL_RELEASE_REQ(msg)) => {
            ll.lch.remove(&msg);
            Ll::send_to_rrm(ToRrm::LL_RELEASE_CNF(ReleaseCnf {}));
        }
    }
}
