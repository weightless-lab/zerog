# ZeroG

**ZeroG** is a free and open-source [weightless](https://en.wikipedia.org/wiki/Weightless_(wireless_communications)) protocol stack.

Currently, this project is at its early stage, nonetheless it includes:

**wed** - An End Device application implemented in (almost) rust-lang featuring all software layers from HW drivers to AT command interperter and running on a [HW board](./hardware/vdb/README.md) designed for this project.

**wbs_simulator** - A simple base station simulator implemented in C running on a SDR platform.

So far, **wed** device is able to 
- receive some basic AT commands (AT+CFUN=1, AT+CREG=1)
- perform a cell search
- camp on a detected cell
- schedule some periodic RF RX activity using sleep/deepsleep features
- decode some downlink channels broacasted by wbs simulator such as (SIB0, DL_RA, UL_RA)

The design is still in progress and the focus was on PCB design/HW bring up and RF driver coding, so the protocol stack is still very basic.

## Build dependencies

- Install rust-lang: https://www.rust-lang.org/tools/install
- Install thumbv7em target: rustup target add thumbv7em-none-eabihf
- Install cargo-embed: https://github.com/probe-rs/cargo-embed
- Install libfec: https://github.com/quiet/libfec
- Install liquid-dsp: https://github.com/jgaeddert/liquid-dsp


* Install required packages: gcc toolchains, C libraries and misc tools.
```
# apt-get install \
        gcc-multilib \
        gcc-arm-none-eabi \
        libnewlib-arm-none-eabi \
        automake autoconf \
        libuhd-dev \
        ;
```

## Compile and run the apps

```
$ git clone https://gitlab.com/weightless-lab/zerog

$ cd apps/wbs_simulator
$ make build
$ ./wbs_simulator

$ cd apps/wed/
$ cargo-embed --release
```


Then, you should be able to launch the wbs_simulator and flash your end device, finally you should end up with the following outputs.

![](./docs/demo_tty.gif)






