#![no_std]

use heapless::Vec;

/* TODO code below is temporary, to be revamped soon */ 

extern "C" {
    pub fn create_viterbi27(len: cty::c_int) -> *mut cty::c_void;
    pub fn init_viterbi27(vp: *const cty::c_void, starting_state: cty::c_int) -> cty::c_int;
    pub fn delete_viterbi27(vp: *const cty::c_void);
    pub fn update_viterbi27_blk(
        vp: *const cty::c_void,
        data: *const cty::c_uchar,
        len: cty::c_int,
    ) -> cty::c_int;
    pub fn chainback_viterbi27(
        vp: *const cty::c_void,
        data: *mut cty::c_uchar,
        len: cty::c_int,
        state: cty::c_int,
    ) -> cty::c_int;
    pub fn interleaver2_encode(
        data_len: cty::c_int,
        data_in: *const cty::c_uchar,
        data_out: *mut cty::c_uchar,
    );
    pub fn crc16(data: *const cty::c_uchar, data_len: cty::c_ushort) -> cty::c_ushort;
}

pub fn decode(is_fec: bool, seed: u16, symbols: &mut [u8]) -> Result<Vec<u8, 256>, u8> {
    if is_fec {
    let vp;
    let mut output_data = Vec::<u8, 256>::new();

    unsafe {
        output_data.set_len(256);
        vp = create_viterbi27(224);
        init_viterbi27(vp, 0);
    }

    let mut dein = Vec::<u8, 256>::new();
    dein.resize(symbols.len(), 0).ok();

    whitening_decode(seed, symbols);

    unsafe {
        interleaver2_encode(symbols.len() as i32, symbols.as_ptr(), dein.as_mut_ptr());
    }

        let mut symbols_bits = Vec::<u8, 512>::new();
        symbols_bits.resize(512, 0).ok();

    unpack_bytes(&dein, &mut symbols_bits);

    for byte in symbols_bits.iter_mut() {
        if *byte == 1 {
            *byte = 255;
        }
    }

    unsafe {
            update_viterbi27_blk(vp, symbols_bits.as_ptr(), 24 + 200 + 6);
            chainback_viterbi27(vp, output_data.as_mut_ptr(), 24 + 200, 0);

        delete_viterbi27(vp);
    }
        let buf: Vec<u8, 256> = Vec::from_slice(&output_data[0..25]).unwrap();

        return Ok(buf);
    } else {
        let buf: Vec<u8, 256> = Vec::from_slice(symbols).unwrap();
        Ok(buf)
    }
}

fn whitening_encode(mut state: u16, data_in: &mut [u8]) {
    let mut data_out: Vec<u8, 255> = Vec::new();

    data_out.push(data_in[0] ^ ((state & 0xff) as u8)).unwrap();

    for data in &data_in[1..] {
        for _ in 0..8 {
            state = (state >> 1) + (((state & 1) ^ (state >> 5) & 1) << 8);
        }
        data_out.push(data ^ ((state & 0xff) as u8)).unwrap();
    }

    for (i, data) in data_out.iter().enumerate() {
        data_in[i] = *data;
    }
}

fn whitening_decode(state: u16, data: &mut [u8]) {
    whitening_encode(state, data);
}

fn unpack_bytes(sym_in: &[u8], sym_out: &mut [u8]) {
    let mut n = 0;

    for byte in sym_in.iter() {
        sym_out[n] = (byte >> 7) & 0x01;
        n += 1;
        sym_out[n] = (byte >> 6) & 0x01;
        n += 1;
        sym_out[n] = (byte >> 5) & 0x01;
        n += 1;
        sym_out[n] = (byte >> 4) & 0x01;
        n += 1;
        sym_out[n] = (byte >> 3) & 0x01;
        n += 1;
        sym_out[n] = (byte >> 2) & 0x01;
        n += 1;
        sym_out[n] = (byte >> 1) & 0x01;
        n += 1;
        sym_out[n] = byte & 0x01;
        n += 1;
    }
}
