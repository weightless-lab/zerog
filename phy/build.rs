fn main() {
    // Put the linker script somewhere the linker can find it

    cc::Build::new()
        .file("viterbi/fec.c")
        .file("viterbi/viterbi27.c")
        .file("viterbi/viterbi27_port.c")
        .file("interleaver/interleaver.c")
        .file("crc/crc16.c")
        .compile("fec");

    println!("cargo:rerun-if-changed=viterbi/fec.c");
    println!("cargo:rerun-if-changed=viterbi/viterbi27_port.c");
    println!("cargo:rerun-if-changed=viterbi/viterbi27.c");
    println!("cargo:rerun-if-changed=interleaver/interleaver.c");
    println!("cargo:rerun-if-changed=crc/crc16.c");
}
