void interleaver2_encode(int data_len, unsigned char *data_in, unsigned char *data_out) {

	for (int i=0; i<data_len;i += 2) {

		data_out[i + 0] = 
			(((data_in[i + 1] >> 0) & 1) << 7) + (((data_in[i + 1] >> 4) & 1) << 6) +
			(((data_in[i + 0] >> 0) & 1) << 5) + (((data_in[i + 0] >> 4) & 1) << 4) +
			(((data_in[i + 1] >> 1) & 1) << 3) + (((data_in[i + 1] >> 5) & 1) << 2) +
			(((data_in[i + 0] >> 1) & 1) << 1) + (((data_in[i + 0] >> 5) & 1) << 0);

		data_out[i + 1] = 
			(((data_in[i + 1] >> 2) & 1) << 7) + (((data_in[i + 1] >> 6) & 1) << 6) +
			(((data_in[i + 0] >> 2) & 1) << 5) + (((data_in[i + 0] >> 6) & 1) << 4) +
			(((data_in[i + 1] >> 3) & 1) << 3) + (((data_in[i + 1] >> 7) & 1) << 2) +
			(((data_in[i + 0] >> 3) & 1) << 1) + (((data_in[i + 0] >> 7) & 1) << 0);
	}
}
