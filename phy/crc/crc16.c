#include <stdint.h>

unsigned short crc16(const uint8_t *src, int len)
		{
   int  crc;
   char i;
   crc = 0;
   while (--len >= 0) {
      crc = crc ^ (int) *src++ << 8;
      i = 8;
      do
      {
         if (crc & 0x8000)
            crc = crc << 1 ^ 0x1021;
         else
            crc = crc << 1;
      } while(--i);
		}
   return (crc ^ 0xffff);
}
